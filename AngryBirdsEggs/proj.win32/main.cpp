#include "main.h"
#include "AppDelegate.h"
#include "cocos2d.h"


USING_NS_CC;

int APIENTRY _tWinMain(HINSTANCE hInstance,
                       HINSTANCE hPrevInstance,
                       LPTSTR    lpCmdLine,
                       int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // create the application instance
    AppDelegate app;

	//CCEGLView* eglView = CCEGLView::create("Gold Rush"); //sharedOpenGLView();
    //eglView->setFrameSize(640, 360); //(480, 320); //(800, 480); //(960, 540); //(960, 640); (1136, 640);

    return Application::getInstance()->run();
}
