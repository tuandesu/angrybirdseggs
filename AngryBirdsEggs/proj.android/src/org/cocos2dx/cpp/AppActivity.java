/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.cpp;

import org.cocos2dx.lib.Cocos2dxActivity;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
//import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;
import com.purplebrain.giftiz.sdk.GiftizSDK;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;

import android.content.ActivityNotFoundException;
//import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.ConnectivityManager;






//import android.telephony.TelephonyManager;
import java.io.File;

import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import android.util.Log;


//Cocos2dxActivity
public class AppActivity extends Cocos2dxActivity {
	
	/**
	 * Save the current instance of Activity, static variables
	 */
	//private static Cocos2dxActivity mActivity = null;
	private static AppActivity mActivity = null;
		
	/** The view to show the ad. */
	private AdView adView;
	
	/* Your ad unit id. Replace with your actual ad unit id. ca-app-pub-3517302379729500/5228017876 */
	private static final String AD_UNIT_ID = "ca-app-pub-3517302379729500/6641568675";
	
	
	private StartAppAd startAppAd = new StartAppAd(this);
	
	// Screenshots path
	private static String scrshotImgPath = "";

	// Helper get display screen to avoid deprecated function use
	private Point getDisplaySize(Display d)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            return getDisplaySizeGE11(d);
        }
        return getDisplaySizeLT11(d);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private Point getDisplaySizeGE11(Display d)
    {
        Point p = new Point(0, 0);
        d.getSize(p);
        return p;
    }
    
    private Point getDisplaySizeLT11(Display d)
    {
        try
        {
            Method getWidth = Display.class.getMethod("getWidth", new Class[] {});
            Method getHeight = Display.class.getMethod("getHeight", new Class[] {});
            return new Point(((Integer) getWidth.invoke(d, (Object[]) null)).intValue(), ((Integer) getHeight.invoke(d, (Object[]) null)).intValue());
        }
        catch (NoSuchMethodException e2) // None of these exceptions should ever occur.
        {
            return new Point(-1, -1);
        }
        catch (IllegalArgumentException e2)
        {
            return new Point(-2, -2);
        }
        catch (IllegalAccessException e2)
        {
            return new Point(-3, -3);
        }
        catch (InvocationTargetException e2)
        {
            return new Point(-4, -4);
        }
    }

    
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocos2dx.lib.Cocos2dxActivity#onCreate(android.os.Bundle)
	 */
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = this;
		
		// =========== Google AdMob =========== //
//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//
//		int width = getDisplaySize(getWindowManager().getDefaultDisplay()).x;
//		//int height = getDisplaySize(getWindowManager().getDefaultDisplay()).y;
//
//
//		LinearLayout.LayoutParams adParams = new LinearLayout.LayoutParams(
//		width,
//		LinearLayout.LayoutParams.WRAP_CONTENT);
//
//
//		adView = new AdView(this);
//		adView.setAdSize(new AdSize(AdSize.FULL_WIDTH, AdSize.AUTO_HEIGHT)); //AdSize.AUTO_HEIGHT, BANNER, WIDE_SKYSCRAPER
//		adView.setAdUnitId(AD_UNIT_ID);
//
//		AdRequest adRequest = new AdRequest.Builder()
//											.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//											.addTestDevice("790E1770F0BCFAFFAF2C9E99EA242F5F")
//											.build();
//
//		adView.loadAd(adRequest);
//        adView.setBackgroundColor(Color.BLACK);
//        addContentView(adView,adParams);

        
        // ====== For AdBuddiz ===== //
        AdBuddiz.setPublisherKey("d8631f98-ed94-40ec-8b35-e32cd667ac00");
        AdBuddiz.cacheAds(this); // this = current Activity
        
        // ===== StartApp =====//
        StartAppSDK.init(this, "109532123", "209500635", true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocos2dx.lib.Cocos2dxActivity#onCreateView()
	 */
	public Cocos2dxGLSurfaceView onCreateView() {
		Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
		// hello should create stencil buffer
		glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);

		return glSurfaceView;
	}

    

	
	
	/**
	 * Set the image, directly open the share panel
	 * @param path (setImagePath)
	 */
	public static void openShare(String path) {
		scrshotImgPath = path;
		// Open the share panel
		//openShare();
		Log.d("", "### in startShare() method");
		//=================================
		
		mActivity.runOnUiThread(new Runnable() {
		    public void run() {
		    	Log.w("A", "share");
		    	Log.d("", "### Settings screenshot 123");
		    	
		        Intent intent=new Intent(android.content.Intent.ACTION_SEND);
		        intent.setType("image/*");
		        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

		        File file = new File(scrshotImgPath); //Ex: "/sdcard/dodgegame.jpg"
		        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
		        intent.putExtra(Intent.EXTRA_TEXT, "My new result of Angry Birds' Eggs game!");
		        mActivity.startActivity(Intent.createChooser(intent, "How do you want to share?"));
		        
		        Log.d("", "### File path: " + scrshotImgPath);
		        
		        // Share text:
//		        Intent sendIntent = new Intent();
//		        sendIntent.setAction(Intent.ACTION_SEND);
//		        sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send." + scrshotImgPath);
//		        sendIntent.setType("text/plain");
//		        mActivity.startActivity(Intent.createChooser(sendIntent, "Share using"));
		
		    }
		});		
		
	}


	public static void openURL(String url) { 
        Intent intent = new Intent(Intent.ACTION_VIEW);  
        intent.setData(Uri.parse(url));
        mActivity.startActivity(intent);
    }
	
	
	public static void hideAd()
    {
		Log.d("", "hideAd");
		
		mActivity.runOnUiThread(new Runnable()
	     {
	
		     @Override
		     public void run()
		     {
				if (mActivity.adView.isEnabled())
					mActivity.adView.setEnabled(false);
				if (mActivity.adView.getVisibility() != 4 )
					mActivity.adView.setVisibility(View.INVISIBLE);
		     }
	     });

    }


    public static void showAd()
    {
    	 Log.d("", "showAd");
    	 
    	 mActivity.runOnUiThread(new Runnable()
	     {
	
		     @Override
		     public void run()
		     {	
				if (!mActivity.adView.isEnabled())
					mActivity.adView.setEnabled(true);
				if (mActivity.adView.getVisibility() == 4 )
					mActivity.adView.setVisibility(View.VISIBLE);	
		     }
	     });

    }
    
    public static void showAdBuddiz()
    {
    	 Log.d("", "showAdBuddiz");
    	 
    	 mActivity.runOnUiThread(new Runnable()
	     {	
		     @Override
		     public void run()
		     {	
		    	 if (AdBuddiz.isReadyToShowAd(mActivity))
		    	 {
		    		 AdBuddiz.showAd(mActivity); //showAd(this);
		    		 Log.d("", "AdBuddiz is ReadyToShowAd");
		    	 }
		    	 else
		    	 {
		    		 Log.d("", "AdBuddiz is NOT ReadyToShowAd! StartApp is used.");
		    		 //StartApp
		    		 mActivity.startAppAd.showAd();
		    		 mActivity.startAppAd.loadAd();		    		 
		    	 }		    	 
		     }
	     });

    }
     
     
    
     /**
      * Get the network info
      * @param context
      * @return
      */
     public static NetworkInfo getNetworkInfo(Context context){
         ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
         return cm.getActiveNetworkInfo();
     }
     
	/**
	 * Check if there is any connectivity
	 * @param context
	 * @return
	 */
	public static boolean isConnected(Context context){
	    NetworkInfo info = AppActivity.getNetworkInfo(context);
	    return (info != null && info.isConnected());
	}
	
	public static boolean isNetworkAvailable() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	@Override
	protected void onResume() {
		super.onResume();
		startAppAd.onResume();
		GiftizSDK.onResumeMainActivity(this);
//		if (adView != null) {
//			adView.resume();
//		}
	}

	@Override
	protected void onPause() {
//		if (adView != null) {
//			adView.pause();
//		}
		super.onPause();
		startAppAd.onPause();
		GiftizSDK.onPauseMainActivity(this);
	}

    @Override
    protected void onDestroy() {
     //adView.destroy();
        super.onDestroy();
    }
    
    @Override
    public void onBackPressed() {
        startAppAd.onBackPressed();
        super.onBackPressed();
    }

    // Rate this game
    public static void rateApp()
    {
    	mActivity.runOnUiThread(new Runnable()
	    {	
    		@Override
    		public void run()
		    {
		    	boolean startActFlg = false;
		    	
		    	Intent intent = new Intent(Intent.ACTION_VIEW);
		        //Try Google play
		        intent.setData(Uri.parse("market://details?id=com.ozenes.AngryBirdsEggs"));
		        try
		        {
		        	mActivity.startActivity(intent);        
		        	startActFlg = true;
		        }
		        catch (ActivityNotFoundException e1)
		        {        	
		            try
		            {
		            	//Market (Google play) app seems not installed, let's try to open a webbrowser
		                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.ozenes.AngryBirdsEggs"));
		                startActFlg = true;
		            } 
		            catch (ActivityNotFoundException e2)
		            {
		            	startActFlg = false;
		            }
		            
		        }
		        if (!startActFlg) {
		            
		                //Well if this also fails, we have run out of options, inform the user.
		                Toast.makeText(mActivity, "Could not open Android market, please install the market app.", Toast.LENGTH_SHORT).show();
		            
		        }   
		    }
	    });
    }
    
    public static void missionCompleted()
    {
    	mActivity.runOnUiThread(new Runnable()
	    {	
		     @Override
		     public void run()
		     {
		    	 Log.d("", "missionCompleted");
		    	 GiftizSDK.missionComplete(mActivity);
		     }
	    });
    }
    
    public static void giftButtonClicked()
    {
    	mActivity.runOnUiThread(new Runnable()
	    {	
		     @Override
		     public void run()
		     {
		    	 Log.d("", "giftButtonClicked");
		    	 GiftizSDK.Inner.buttonClicked(mActivity);
		     }
	    });
    }
    
}


