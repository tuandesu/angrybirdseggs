#include "AboutScene.h"
#include "Common/GameManager.h"
#include "Common/ResourceNames.h"
#include "HomeScene.h"

USING_NS_CC;

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

Scene* About::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = About::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool About::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->screenSize = Director::getInstance()->getOpenGLView()->getFrameSize(); 
	this->centerPoint = ccp(screenSize.width/2, screenSize.height/2);

    /////////////////////////////
    
	// ----- add Backgound
    auto spriteBG = Sprite::create(s_bg_common);

    // position the sprite on the center of the screen
    spriteBG->setPosition(this->centerPoint);

    // add the sprite as a child to this layer
    this->addChild(spriteBG, 0);

	// ----- add "About info"
    auto spriteAbout = Sprite::create(s_about_info);

    // position the sprite on the center of the screen
    spriteAbout->setPosition(this->centerPoint.x, this->screenSize.height - spriteAbout->getContentSize().height*0.75);

    // add the sprite as a child to this layer
    this->addChild(spriteAbout, 1);



	// ----- add a "Home" 
    auto homeItem = MenuItemImage::create(
                                           s_btn_home,
                                           s_btn_home_tapped,
                                           CC_CALLBACK_1(About::menuHomeCallback, this));
    	
	homeItem->setPosition(this->centerPoint.x, homeItem->getContentSize().height*0.6);

    // create menu, it's an autorelease object
    auto menu = Menu::create(homeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 2, 2);

    
    return true;
}


void About::menuHomeCallback(Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}

	CCTransitionFade* transition = CCTransitionFade::create(1, Home::createScene(), ccBLACK);
    CCDirector::getInstance()->replaceScene(transition);
}
