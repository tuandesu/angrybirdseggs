#ifndef __PLAYINGRESULT_SCENE_H__
#define __PLAYINGRESULT_SCENE_H__

#include "cocos2d.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	#define SCREENSHOT_FILE_PATH         "/sdcard/AB_Eggs_CaptureScreen.jpg"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	#define SCREENSHOT_FILE_PATH         "AB_Eggs_CaptureScreen.jpg"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
	#define SCREENSHOT_FILE_PATH         "AB_Eggs_CaptureScreen.jpg"
#else
	#define SCREENSHOT_FILE_PATH         "AB_Eggs_CaptureScreen.jpg"//(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#endif

USING_NS_CC;

class PlayingResult : public cocos2d::Layer
{
private:
	Size screenSize;
	//Size visibleSize;
	Point centerPoint;
	float screenRatio;

	int pinStarCounter;

	int pressedReplayCounter;
	//bool adsIsShowing;

	Menu* rateMenu;
	bool isRated;

	void createRateMn();
	void hideRateMn();
	void showRateThisGame(float dt);

	void showNewBestResult();
	void showFailedResult();

	void fireworksEffect();
	void fireworksEffect2(float dt);

	void pinStarSound(float dt);

	//void rateThisGame();

	void capture();
	void afterCaptured(bool succeed, const std::string& outputFile);

    //void showExcellentSeal(float dt);
	void goToPlayScene(float dt);


public:
	
	
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callback: Classic
    void menuReplayCallback(cocos2d::Ref* pSender);

	// a selector callback: Zen
    void menuShareCallback(cocos2d::Ref* pSender);

	// a selector callback: More
    void menuHomeCallback(cocos2d::Ref* pSender);
    
    void menuBlackBgCallback(cocos2d::Ref* pSender);

    // a selector callback: Yes
	void menuYesCallback(cocos2d::Ref* pSender);

	// a selector callback: No
	void menuNoCallback(cocos2d::Ref* pSender);

    // implement the "static create()" method manually
    CREATE_FUNC(PlayingResult);
};

#endif // __PLAYINGRESULT_SCENE_H__
