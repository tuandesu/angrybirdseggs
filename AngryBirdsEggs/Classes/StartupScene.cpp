#include "StartupScene.h"
#include "Common/GameManager.h"
#include "Common/ResourceNames.h"
#include "HomeScene.h"

USING_NS_CC;

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

Scene* Startup::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = Startup::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Startup::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->screenSize = Director::getInstance()->getOpenGLView()->getFrameSize(); 
	this->centerPoint = ccp(screenSize.width/2, screenSize.height/2);

    /////////////////////////////
    
	

	// ----- add "Ozenes"
    auto spriteOzenes = Sprite::create(s_ozenes_logo);

    // position the sprite on the center of the screen
    spriteOzenes->setPosition(this->centerPoint.x, this->centerPoint.y + spriteOzenes->getContentSize().height*0.6);
	spriteOzenes->setOpacity(0.0f);

    // add the sprite as a child to this layer
    this->addChild(spriteOzenes, 1, 1);
    
	spriteOzenes->runAction(Sequence::create(FadeIn::create(1.0f), CCDelayTime::create(2.25f), FadeOut::create(0.75f),  NULL));

	// ----- add "Ozenes 3D"
	auto spriteOzenes3D = Sprite::create(s_ozenes_logo_3d);

	// position the sprite on the center of the screen
	spriteOzenes3D->setPosition(spriteOzenes->getPosition());
	spriteOzenes3D->setOpacity(0.0f);

	// add the sprite as a child to this layer
	this->addChild(spriteOzenes3D, 2, 2);

	spriteOzenes3D->runAction(Sequence::create(CCDelayTime::create(1.0f), FadeIn::create(0.75f), CCDelayTime::create(0.75f), FadeOut::create(0.75f),  NULL));

	//===================================================//

    
    // preload background music and effect
    SimpleAudioEngine::getInstance()->preloadBackgroundMusic( BG_MUSIC_FILE );
    SimpleAudioEngine::getInstance()->preloadEffect( BUTTON_TOUCHED_FILE_1 );

    // set default volume
    //SimpleAudioEngine::getInstance()->setEffectsVolume(1.0);
    //SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.75);

	//get from option setting
	GameManager::soundOn = CCUserDefault::getInstance()->getBoolForKey("soundOn", true); 
	GameManager::backgroundMusicOn = CCUserDefault::getInstance()->getBoolForKey("backgroundMusicOn", true); 

	GameManager::themeBg = CCUserDefault::getInstance()->getIntegerForKey("themeBg", 1);  // 1: s_bg_playing_01; 2: s_bg_playing_02;..

	GameManager::bestResult_Level = CCUserDefault::getInstance()->getIntegerForKey("BestResult_Level", 1);
	GameManager::bestResult_Score = CCUserDefault::getInstance()->getIntegerForKey("BestResult_Score", 0);

	//GameManager::gamePlayKinds = GamePlayKind_UNKNOWN;

	if (GameManager::backgroundMusicOn)
		SimpleAudioEngine::getInstance()->playBackgroundMusic(BG_MUSIC_FILE, true);

	//===================================================//

	this->schedule(schedule_selector(Startup::goToHomeScene), 4.0f); //2.5f

    return true;
}

void Startup::goToHomeScene(float dt)
{	
	this->unschedule(schedule_selector(Startup::goToHomeScene));

	// Go to result-scene
	CCTransitionFade* transition = CCTransitionFade::create(1, Home::createScene(), ccBLACK);
    CCDirector::getInstance()->replaceScene(transition);
}
