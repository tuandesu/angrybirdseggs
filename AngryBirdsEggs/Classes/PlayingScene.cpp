#include "PlayingScene.h"
#include "Common/ResourceNames.h"
#include "Common/GameManager.h" 
#include "SimpleAudioEngine.h"
#include "ResultScene.h"
#include <ctime>

USING_NS_CC;

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

Scene* Playing::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = Playing::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Playing::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->screenSize = Director::getInstance()->getOpenGLView()->getFrameSize(); 
	this->centerPoint = ccp(screenSize.width/2, screenSize.height/2);
	this->screenRatio = this->screenSize.height/480; //this->screenSize.width/320;

    /////////////////////////////

	// Set color layer.
	this->colorLayer = CCLayerColor::create(ccc4(0, 0, 0, 200)); 
	//this->colorLayer->setOpacity(0.3f);
	
	// position the sprite on the center of the screen
    this->colorLayer->setPosition(0,0);

	this->addChild(colorLayer, 99, 0);
	this->colorLayer->setVisible(false);

	// Background
	this->parentBG = CCNode::create();
	this->parentBG->setPosition(0,0);
	
	this->addChild(this->parentBG,0,0);

	
    
	// ----- add Backgound 1
    this->spriteBG1 = Sprite::create(s_bg_playing); //"bg_playing_01.jpg"

    // position the sprite on the center of the screen
    this->spriteBG1->setPosition(this->centerPoint);

	// ----- add Backgound 2
    this->spriteBG2 = Sprite::create(s_bg_playing);

    // position the sprite on the center of the screen
    this->spriteBG2->setPosition(Vec2(this->centerPoint.x, this->centerPoint.y + this->spriteBG2->getContentSize().height) );

	// ----- add Backgound 3
    this->spriteBG3 = Sprite::create(s_bg_playing);

    // position the sprite on the center of the screen
    this->spriteBG3->setPosition(Vec2(this->centerPoint.x, this->centerPoint.y + this->spriteBG3->getContentSize().height*2) );

    // add the sprite as a child to this layer
    this->addChild(this->spriteBG1, 3, 1); //parentBG->
	this->addChild(this->spriteBG2, 2, 2);
	this->addChild(this->spriteBG3, 1, 3);
	
	//colorLayer->setAnchorPoint(Vec2(0,0));

	// ----- add Top progress-bar's bg
    Sprite* sTopProgressBarBG = Sprite::create(s_top_progress_bar_bg);
		
    // position the sprite on the center of the screen
	sTopProgressBarBG->setScale(this->screenSize.width/sTopProgressBarBG->getContentSize().width);
    sTopProgressBarBG->setPosition(this->centerPoint.x, this->screenSize.height);	// - sTopProgressBarBG->getContentSize().height/2
	sTopProgressBarBG->setAnchorPoint(Vec2(0.5f, 1.0f));
	this->addChild(sTopProgressBarBG, 4, 4);
		
	// sTopProgressBar back
	Sprite* sTopProgressBarBack = Sprite::create(s_top_progress_bar_back);

    // position the sprite on the center of the screen
	sTopProgressBarBack->setScale(this->screenSize.width/sTopProgressBarBack->getContentSize().width);
    sTopProgressBarBack->setPosition(this->centerPoint.x, this->screenSize.height);	// - sTopProgressBarBack->getContentSize().height/2 
	sTopProgressBarBack->setAnchorPoint(Vec2(0.5f, 1.0f));
	this->addChild(sTopProgressBarBack, 5, 5);
	
	//ProgressBar
    this->topProgressBar = ProgressTimer::create(Sprite::create(s_top_progress_bar_front));
    this->topProgressBar->setType(ProgressTimer::Type::BAR);

	// Setup for a bar starting from the left since the midpoint is 0 for the x
    this->topProgressBar->setMidpoint(Vec2(0,0));
    // Setup for a horizontal bar since the bar change rate is 0 for y meaning no vertical change
    this->topProgressBar->setBarChangeRate(Vec2(1, 0));
    this->addChild(topProgressBar, 6, 6);
    
	this->topProgressBar->setScale(this->screenSize.width/sTopProgressBarBack->getContentSize().width);
	this->topProgressBar->setPosition(this->centerPoint.x, this->screenSize.height); // - sTopProgressBarBack->getContentSize().height/2 
	topProgressBar->setAnchorPoint(Vec2(0.5f, 1.0f));
	this->topProgressBar->setPercentage(0.0f);


	//Playing Level
	auto labelLevel_L = Label::createWithTTF("L", FONT_FEAST_OF_FLESH_BB, 18*this->screenRatio);
	labelLevel_L->setPosition(this->screenSize.width*0.075f, this->screenSize.height - sTopProgressBarBack->getContentSize().height*1.2);
	labelLevel_L->setColor(ccc3(255,210,2));
	this->addChild(labelLevel_L, 7, 7);

	auto labelLevel_evel = Label::createWithTTF("evel", FONT_FEAST_OF_FLESH_BB, 14*this->screenRatio);
	labelLevel_evel->setPosition(labelLevel_L->getPositionX() + labelLevel_L->getContentSize().width*0.5  + labelLevel_evel->getContentSize().width*0.5,
										labelLevel_L->getPositionY() - labelLevel_evel->getContentSize().height*0.1);
	labelLevel_evel->setColor(ccc3(255,210,2));
	this->addChild(labelLevel_evel, 7, 8);

	char textLevel[256];
	sprintf(textLevel,"%d", 1); //GameManager::playingLevel
	
	this->labelPlayingLevel = Label::createWithTTF(textLevel, FONT_FEAST_OF_FLESH_BB, 18*this->screenRatio);
	this->labelPlayingLevel->setPosition(labelLevel_evel->getPositionX() + labelLevel_evel->getContentSize().width*0.9,
										labelLevel_L->getPositionY());
	this->labelPlayingLevel->setColor(ccc3(255,210,2));
	this->addChild(this->labelPlayingLevel, 7, 9);


	//Score/money
	char textScore[256];
	sprintf(textScore,"$ %d", 0);

	this->labelPlayingScores = Label::createWithTTF(textScore, FONT_FEAST_OF_FLESH_BB, 17*this->screenRatio);
	this->labelPlayingScores->setPosition(this->screenSize.width - this->screenSize.width*0.14f, this->screenSize.height - sTopProgressBarBack->getContentSize().height*1.2);
	this->labelPlayingScores->setColor(ccc3(255,210,2));
	this->addChild(this->labelPlayingScores, 8, 8);

	//Level Up
	this->spriteLevelUp = Sprite::create(s_level_up_playing);
	this->spriteLevelUp->setOpacity(0.0f);
	this->spriteLevelUp->setPosition(this->centerPoint.x, this->screenSize.height*0.5f); //0.6
	this->addChild(this->spriteLevelUp, 16, 16);


	// Init game items
	this->initGameItems();

	this->addGoButton();

	//this->addCtrlButtons();

	if (GameManager::backgroundMusicOn)
		SimpleAudioEngine::getInstance()->playBackgroundMusic(BG_MUSIC_PLAYING_FILE, true);

	//Application::showAdBuddiz();

    return true;
}



void Playing::initGameItems()
{
	//CCSprite* spriteBG = CCSprite::create();

	CCSize bgSize = this->screenSize; //this->spriteBG1->getContentSize();
	this->nestWidth = 0;
	this->ctrlHeight = 0;
	this->eggSizeWidth = 0;
	this->eggSizeHeight = 0;
	this->pigSizeWidth = 0;
	this->pigSizeHeight = 0;
	this->minCollisionDistance = 0;

    int minX = 0;
    int maxX = bgSize.width;
	this->oneUnitX = maxX/GameManager::numberOfColumns;

	int minY = 0;
    int maxY = bgSize.height;
    this->oneUnitY = maxY/GameManager::numberOfRows;

	//this->eggSizeWidth = oneUnitX;
	//this->eggSizeHeight = oneUnitY;

    srand(time(NULL));
				   
	//Init eggs
    this->initGameObjForOneBG(this->spriteBG1, oneUnitX, oneUnitY, true);
	this->initGameObjForOneBG(this->spriteBG2, oneUnitX, oneUnitY, false);
	this->initGameObjForOneBG(this->spriteBG3, oneUnitX, oneUnitY, false);

	//Init pig
	this->initPig(oneUnitY);
}

void Playing::initGameObjForOneBG(Sprite* spriteBG, int oneUnitX, int oneUnitY, bool isInitial)
{
	// Remove all children before add new
	spriteBG->removeAllChildrenWithCleanup(true);
		
	int iStartAtIdx = 1;
	if (isInitial)	
		iStartAtIdx = GameManager::numberOfRows;

	for (int i = iStartAtIdx; i <= GameManager::numberOfRows; i++) 
	{
		for (int j = 1; j <= GameManager::numberOfColumns; j++) 
		{	
				
			//--- NEST ---//
			std::string tmpName = ""; 
			int nestIdx = (rand() % 3) + 1;
			if (nestIdx == 1)
				tmpName = s_nest_bird_1;
			else if (nestIdx == 2)
				tmpName = s_nest_bird_2;
			else 
				tmpName = s_nest_bird_3;

			auto spriteNest = Sprite::create(tmpName); 

			if (this->nestWidth == 0)
				this->nestWidth = spriteNest->getContentSize().width;

			//Position
			//int posX = j*oneUnitX - oneUnitX/2 + CCRANDOM_MINUS1_1()*spriteNest->getContentSize().width*0.1f; 
			int posX = spriteBG->getContentSize().width/2 + CCRANDOM_MINUS1_1()*spriteNest->getContentSize().width*0.1f;
			int posY = i*oneUnitY - oneUnitY/2 + CCRANDOM_MINUS1_1()*spriteNest->getContentSize().height*0.15f; //0.05f
			if ((j == 1) || (j == 3))
			{
				posY = posY + oneUnitY/2;
				if (j == 1)
					posX = posX - spriteNest->getContentSize().width*0.9f + CCRANDOM_MINUS1_1()*spriteNest->getContentSize().width*0.1f;

				if (j == 3)
					posX = posX + spriteNest->getContentSize().width*0.9f + CCRANDOM_MINUS1_1()*spriteNest->getContentSize().width*0.1f;
			}
			spriteNest->setPosition(posX, posY);				
			
			//Rotation
			float deltaAngle = 40 * CCRANDOM_MINUS1_1();
			spriteNest->setRotation(deltaAngle);
						
			spriteBG->addChild(spriteNest, 1, GameManager::nestBird_Tag); 

			//--- Add Eggs ---//
			int tmpRand = (rand() % 12) + 1;
			int numOfEggs = 1;
			if (tmpRand <= 2)
				numOfEggs = 3;
			else if (tmpRand <= 6)
				numOfEggs = 2;

			for (int k = 1; k <= numOfEggs; k++) 
			{
				int eggZOrder = 2;
				float deltaAngleEgg = 0.0f;

				//Create egg
				int nameIdx = (rand() % 20) + 1;
				tmpName = GameManager::getFileNameByIdx(nameIdx);
				auto spriteEgg = Sprite::create(tmpName);

				if (eggSizeWidth == 0)
				{
					eggSizeWidth = spriteEgg->getContentSize().width;
					eggSizeHeight = spriteEgg->getContentSize().height;
				}

				//Position
				int eggPosX = posX;
				int eggPosY = posY;

				if (numOfEggs == 1)
				{
					//Rotation
					deltaAngleEgg = 30 * CCRANDOM_MINUS1_1();
				}
				else if (numOfEggs == 2)
				{
					if (k == 1)
					{
						eggPosX = eggPosX - spriteEgg->getContentSize().width*0.5f;
						//Rotation
						deltaAngleEgg = -1*(20 + (15 * CCRANDOM_0_1()));
					}
					else if (k == 2)
					{
						eggPosX = eggPosX + spriteEgg->getContentSize().width*0.5f;
						//Rotation
						deltaAngleEgg = 20 + (15 * CCRANDOM_0_1());
					}
					eggPosY = eggPosY + CCRANDOM_MINUS1_1()*spriteEgg->getContentSize().width*0.1f; 
				} else if (numOfEggs == 3)
				{
					if (k == 1)
					{
						eggPosX = eggPosX - spriteEgg->getContentSize().width*0.5f;
						eggPosY = eggPosY - spriteEgg->getContentSize().width*0.4f;
						eggZOrder = 3;

						//Rotation
						deltaAngleEgg = -1*(25 + (20 * CCRANDOM_0_1()));
					}
					else if (k == 2)
					{
						//eggPosX = eggPosX;
						eggPosY = eggPosY + spriteEgg->getContentSize().width*0.45f;

						//Rotation
						deltaAngleEgg = 15 * CCRANDOM_MINUS1_1();
					}
					else if (k == 3)
					{
						eggPosX = eggPosX + spriteEgg->getContentSize().width*0.5f;
						eggPosY = eggPosY - spriteEgg->getContentSize().width*0.4f;
						eggZOrder = 4;

						//Rotation
						deltaAngleEgg = 25 + 20 * CCRANDOM_0_1();
					}
				}
				spriteEgg->setPosition(eggPosX, eggPosY);
				spriteEgg->setRotation(deltaAngleEgg);
				
				//create Tag
				int eggTag = GameManager::getTagByIdx(nameIdx);

				//add egg
				spriteBG->addChild(spriteEgg, eggZOrder, eggTag);
			}
		}
	}
}

void Playing::initPig(int oneUnitY)
{
	this->spritePig = Sprite::create(s_pig1);
	this->spritePig->setPosition(this->centerPoint.x, this->screenSize.height*0.33f);
	this->addChild(spritePig, 9, 9);

	if (this->pigSizeWidth == 0){
		this->pigSizeWidth = this->spritePig->getContentSize().width;
		this->pigSizeHeight = this->spritePig->getContentSize().height;

		this->minCollisionDistance = this->eggSizeWidth + this->pigSizeWidth;
	}
}

void Playing::pigRunning()
{
	CCAnimation * anim = CCAnimation::create();
	// There are other several ways of storing + adding frames,
	// this is the most basic using one image per frame.
	anim->addSpriteFrameWithFileName(s_pig1);
	anim->addSpriteFrameWithFileName(s_pig2);
	anim->addSpriteFrameWithFileName(s_pig3);
	anim->addSpriteFrameWithFileName(s_pig4);
	anim->addSpriteFrameWithFileName(s_pig5);
	anim->addSpriteFrameWithFileName(s_pig4);
	anim->addSpriteFrameWithFileName(s_pig3);
	anim->addSpriteFrameWithFileName(s_pig2);

	anim->setLoops(-1); // for infinit loop animation
	anim->setDelayPerUnit(0.075f); //Duration per frame
	//CCAnimate *theAnim = CCAnimate::actionWithDuration(1.8f,anim,true); // this wont work in newer version..

	this->spritePig->runAction(CCAnimate::create(anim) );
}

void Playing::addGoButton()
{
	// ----- add a "Go" 
    auto goItem = MenuItemImage::create(
                                           s_btn_go,
                                           s_btn_go_tapped,
                                           CC_CALLBACK_1(Playing::menuGoCallback, this));
    	
	goItem->setPosition(Vec2(this->centerPoint.x, this->spritePig->getPositionY() - goItem->getContentSize().height*0.5));
		
    // create menu, it's an autorelease object
    this->menuGo = Menu::create(goItem, NULL);
    this->menuGo->setPosition(Vec2::ZERO);
    this->addChild(this->menuGo, 10, 10);
}



void Playing::menuGoCallback(Ref* pSender)
{
	if (GameManager::soundOn)
	{
		SimpleAudioEngine::getInstance()->playEffect(LET_GO_FILE, false, 1, 0, 1);
	}

	//this->menuGo->setVisible(false);
	this->menuGo->setEnabled(false);
	//this->menuPigCtrl->setEnabled(true);

	this->menuGo->runAction(FadeOut::create(0.25f));
	//this->menuPigCtrl->runAction(FadeIn::create(0.25f));

	// Start game
	this->startGame();	

}


void Playing::updateGameScolling(float dt)
{	

	int posY1 = this->spriteBG1->getPosition().y; //this->spriteBG1->convertToWorldSpace(
	int posY2 = this->spriteBG2->getPosition().y;
	int posY3 = this->spriteBG3->getPosition().y;

	if (posY1 <= 0 - this->spriteBG1->getContentSize().height) {		
		this->spriteBG1->setPositionY(this->spriteBG1->getPositionY() + this->spriteBG1->getContentSize().height*3);
		this->spriteBG1->setZOrder(1);
		this->spriteBG3->setZOrder(2);
		this->spriteBG2->setZOrder(3);

		//Reset all new children
		this->initGameObjForOneBG(this->spriteBG1, this->oneUnitX, this->oneUnitY, false);
	}

	if (posY2 <= 0 - this->spriteBG2->getContentSize().height) {		
		this->spriteBG2->setPositionY(this->spriteBG2->getPositionY() + this->spriteBG2->getContentSize().height*3);
		this->spriteBG2->setZOrder(1);
		this->spriteBG1->setZOrder(2);
		this->spriteBG3->setZOrder(3);

		//Reset all new children
		this->initGameObjForOneBG(this->spriteBG2, this->oneUnitX, this->oneUnitY, false);
	}

	if (posY3 <= 0 - this->spriteBG3->getContentSize().height) {
		this->spriteBG3->setPositionY(this->spriteBG3->getPositionY() + this->spriteBG3->getContentSize().height*3);
		this->spriteBG3->setZOrder(1);
		this->spriteBG2->setZOrder(2);
		this->spriteBG1->setZOrder(3);

		//Reset all new children
		this->initGameObjForOneBG(this->spriteBG3, this->oneUnitX, this->oneUnitY, false);
	}

	// update scrolling speed
	//this->changeScrollingSpeed(false);
}

void Playing::startGame()
{
	//GameManager::gamePlayKinds = GamePlayKind_RUSH;

	this->isGameFinished = false;
	this->isMoved = false;
	this->isJumpingForward = false;
	this->isCheckingCollision = false;

	GameManager::playingScore = 0;
	GameManager::playingLevel = 1;
	this->targetScoreForCurrLevel = GameManager::calculateLevelTarget(GameManager::playingLevel);

	this->scorePosY1 = 0;
	this->scorePosY2 = 0;

	//this->currentTimeSpeed = 1.75f;
	//this->slowestSpeed = this->currentTimeSpeed;

	this->missionCompleted = CCUserDefault::getInstance()->getBoolForKey("MissionCompleted", false);

	this->gravityY = GameManager::calculateSpeedForwardOfPig(GameManager::playingLevel) * this->screenRatio; //-1.5, -2.5, -3.5

	/*if (this->screenSize.height <= resource_480x800.size.height)
	{
		this->gravityY = this->gravityY * 1.5f;
	}*/

	//this->slowestSpeedStep = this->gravityY;
	this->gravityX = 0.0f;


	//for change scrolling speed
	struct timeval now;
	gettimeofday(&now, nullptr);
	this->lastUpdate = now;
		
	//start game at (for played time)
	this->startGameAt = now;

	GameManager::isFailedGame = false;

	// Update scolling speed
    schedule(schedule_selector(Playing::updateGameScolling), 0.25f); //0.25f

    // Pig start running (animation only)
    this->pigRunning();

    // for pig moving
    this->scheduleUpdate();
	//this->schedule(schedule_selector(Playing::updatePigRunning), 0.017f); // 0.041f = 24scenes/second

	this->schedule(schedule_selector(Playing::checkingCollision), 0.1f);

    this->setTouchEnabled(true);
}



void Playing::gameOver(Point bombPos)
{
	//this->isGameFinished = true;
	//this->setTouchEnabled(false);
	GameManager::isFailedGame = true;

	//this->unschedule(schedule_selector(Playing::updateGameScolling));

	this->gameFinish();

	// === Explosion 1 ===
	// Play a particle effect when the enemy was destroyed
	ParticleSystem* system1 = ParticleSystemQuad::create("particles/fx-explosion2.plist");

	// Set some parameters that can't be set in Particle Designer
	system1->setPositionType(kCCPositionTypeFree);
	system1->setAutoRemoveOnFinish(true);
	system1->setPosition(bombPos);
	this->addChild(system1, 100, 100);

	// === Explosion 2 ===
	// Play a particle effect when the enemy was destroyed
	ParticleSystemQuad* system2 = ParticleSystemQuad::create("particles/ExplodingRing.plist");
	system2->setTextureWithRect(Director::getInstance()->getTextureCache()->addImage("particles/particles.png"), Rect(0,0,32,32));
	// Set some parameters that can't be set in Particle Designer
	system2->setPositionType(kCCPositionTypeFree);
	system2->setAutoRemoveOnFinish(true);
	system2->setPosition(bombPos);
	this->addChild(system2, 100, 100);
	//system2->retain();

	// === Explosion 3 ===
	// Play a particle effect when the enemy was destroyed
	ParticleSystem* system3 = ParticleSystemQuad::create("particles/LavaFlow.plist");

	// Set some parameters that can't be set in Particle Designer
	system3->setPositionType(kCCPositionTypeFree);
	system3->setAutoRemoveOnFinish(true);
	system3->setPosition(bombPos);

	system3->setDuration(0.5);
	system3->setSpeed(300*this->screenRatio);
	system3->setSpeedVar(25*this->screenRatio);

	this->addChild(system3, 100, 100);


	// Play sound
	if (GameManager::soundOn)
		SimpleAudioEngine::getInstance()->playEffect(BOMB_EFFECT_FILE_1, false, 1, 0, 1);
		
	this->colorLayer->setOpacity(0.0f);
	this->colorLayer->setVisible(true);

	CCActionInterval*  fadeIn = CCFadeIn::create(0.5f);
	this->colorLayer->runAction(fadeIn);

	// go to result scene
	this->schedule(schedule_selector(Playing::goToResultScene), 1.25f);

	// Show "Game Over!"
    //schedule(schedule_selector(Playing::showGameOver), 1.25f);
}


void Playing::gameFinish()
{
	this->setTouchEnabled(false);

	this->isGameFinished = true;
	
	this->unschedule(schedule_selector(Playing::updateGameScolling));
	this->unscheduleUpdate();
	//this->unschedule(schedule_selector(Playing::updatePigRunning));
	this->unschedule(schedule_selector(Playing::checkingCollision));

	this->spriteBG1->stopAllActions();
	this->spriteBG2->stopAllActions();
	this->spriteBG3->stopAllActions();

	if (SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
	{
		SimpleAudioEngine::getInstance()->stopBackgroundMusic();		
	}
}

void Playing::goToResultScene(float dt)
{	
	this->unschedule(schedule_selector(Playing::goToResultScene));

	// Go to result-scene
	CCTransitionFade* transition = CCTransitionFade::create(1, PlayingResult::createScene(), ccBLACK);
    CCDirector::getInstance()->replaceScene(transition);
}



void Playing::onEnter()
{
      
	// must call super here:
	CCLayer::onEnter(); 
	
	// Register Touch Event
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(Playing::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(Playing::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(Playing::onTouchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}


bool Playing::onTouchBegan(Touch* touch, Event* event)
{
    
	if (!this->isGameFinished)
	{
		this->beginPos = touch->getLocation();
		this->isMoved = false;
		this->isJumpingForward = false;
	}

    return true;
}

void Playing::touchedOnGoldEffect(Point pigPos)
{
	ParticleSystem* sysEffect = ParticleFlower::create();
    //sysEffect->retain();
    this->addChild(sysEffect, 10);
    sysEffect->setTexture( Director::getInstance()->getTextureCache()->addImage(s_star3_effect) );

    sysEffect->setPosition(pigPos);
	sysEffect->setAutoRemoveOnFinish(true);
	sysEffect->setDuration(0.4f);
	sysEffect->setLife(0.45f);
	sysEffect->setLifeVar(0.25f);
	sysEffect->setSpeed(140*this->screenRatio);
	sysEffect->setSpeedVar(40*this->screenRatio);
	sysEffect->setGravity(Vec2(0, -300*this->screenRatio));
}


void Playing::onTouchMoved(Touch* touch, Event* event)
{
    // If it weren't for the TouchDispatcher, you would need to keep a reference
    // to the touch from touchBegan and check that the current touch is the same
    // as that one.
    // Actually, it would be even more complicated since in the Cocos dispatcher
    // you get Sets instead of 1 UITouch, so you'd need to loop through the set
    // in each touchXXX method.
    
    //CCLOG("Paddle::onTouchMoved id = %d, x = %f, y = %f", touch->getID(), touch->getLocation().x, touch->getLocation().y);
    
	auto touchPoint = touch->getLocation();

	this->slideToLeftRight(touchPoint);
}


void Playing::onTouchEnded(Touch* touch, Event* event)
{
	auto touchEndedPoint = touch->getLocation();

	this->slideToLeftRight(touchEndedPoint);

	if (!this->isMoved)
	{
		if ((abs(this->beginPos.y - touchEndedPoint.y) < this->screenSize.width/16)
			&& (abs(this->beginPos.x - touchEndedPoint.x) < this->screenSize.width/16))
		{
			if ((touchEndedPoint.x < this->spritePig->getPositionX())
					&& (this->beginPos.x < this->spritePig->getPositionX()))
			{
				this->goToLeft();
			} else if ((touchEndedPoint.x > this->spritePig->getPositionX())
					&& (this->beginPos.x > this->spritePig->getPositionX()))
			{
				this->goToRight();
			}
		}
	}

	if (this->isMoved)
		this->schedule(schedule_selector(Playing::updatePigSpeed), 0.2f);
	else
		this->checkAndJumpForward(touchEndedPoint);

}

void Playing::slideToLeftRight(Point touchPoint)
{
	//if ((touchPoint.y >= this->ctrlHeight) && (this->beginPos.y >= this->ctrlHeight))
	//{
	if (abs(this->beginPos.y - touchPoint.y) <= abs(this->beginPos.x - touchPoint.x)*0.65)
	{
		if (this->beginPos.x > touchPoint.x)
		{
			if (this->beginPos.x - touchPoint.x >= this->screenSize.width/16)
			{
				this->beginPos = touchPoint;
				this->goToLeft();
			}
		} else {
			if (touchPoint.x - this->beginPos.x >= this->screenSize.width/16)
			{
				this->beginPos = touchPoint;
				this->goToRight();
			}
		}
	}
	//}
}

void Playing::checkAndJumpForward(Point endedPoint)
{
	if ((endedPoint.y - this->beginPos.y) >= abs(this->beginPos.x - endedPoint.x)*1.35)
	{
		if (endedPoint.y - this->beginPos.y >= this->screenSize.height/8)
		{
			this->isJumpingForward = true;

			ActionInterval *scaleTo = ScaleTo::create(0.15f, 1.4f);
			ActionInterval *scaleToBack = ScaleTo::create(0.15, 1.0f);
			auto jumpFinished = CallFunc::create(CC_CALLBACK_0(Playing::jumpForwardFinished, this));
			Sequence *actSequence = (Sequence*)Sequence::create(scaleTo, CCDelayTime::create(0.4f), scaleToBack, jumpFinished, NULL); //CCDelayTime::create(1.0f), fadeOut,

			this->spritePig->runAction(actSequence);

			this->setTouchEnabled(false);
		}
	}
}

void Playing::jumpForwardFinished()
{
	this->isJumpingForward = false;
	this->setTouchEnabled(true);
}

void Playing::updatePigSpeed(float delta)
{
	if (this->gravityX > 0)
	{
		if (this->gravityX > 1.0f)
			this->gravityX = 1.0f * this->screenRatio;
		else if (this->gravityX > 0.7f)
			this->gravityX = 0.7f * this->screenRatio;
		else
		{
			this->gravityX = 0.35f * this->screenRatio;
			this->unschedule(schedule_selector(Playing::updatePigSpeed));
		}
	} else {
		if (this->gravityX < -1.0f)
			this->gravityX = -1.0f * this->screenRatio;
		else if (this->gravityX < -0.7f)
			this->gravityX = -0.7f * this->screenRatio;
		else
		{
			this->gravityX = -0.35f * this->screenRatio;
			this->unschedule(schedule_selector(Playing::updatePigSpeed));
		}
	}

	this->spritePig->setRotation(1.2f*this->gravityX);
}

//PigRunning
void Playing::update(float delta)
{
	Point pigPos = this->spritePig->getPosition();

	if (pigPos.x >= this->screenSize.width - this->spritePig->getContentSize().width*0.4f)
	{
		if (this->gravityX > 0)
		{
			this->gravityX = -0.35f*this->screenRatio;
			this->spritePig->setPositionX(this->screenSize.width - this->spritePig->getContentSize().width*0.4f);

			this->spritePig->setRotation(1.2*this->gravityX);
		}
		//this->gravityX = -1*this->gravityX;
	}

	if (pigPos.x <= this->spritePig->getContentSize().width*0.4f)
	{
		if (this->gravityX < 0)
		{
			this->gravityX = 0.35f*this->screenRatio;
			this->spritePig->setPositionX(this->spritePig->getContentSize().width*0.4f);

			this->spritePig->setRotation(1.2*this->gravityX);
		}
	}

	//Pig
	this->spritePig->setPosition(ccpAdd(pigPos, ccp(this->gravityX, 0)));

	//BG
	this->spriteBG1->setPosition(ccpAdd(this->spriteBG1->getPosition(), ccp(0, this->gravityY)));
	this->spriteBG2->setPosition(ccpAdd(this->spriteBG2->getPosition(), ccp(0, this->gravityY)));
	this->spriteBG3->setPosition(ccpAdd(this->spriteBG3->getPosition(), ccp(0, this->gravityY)));
}

bool Playing::checkCollisionWithEggs(Sprite* spriteBG, Point pigPos)
{
	bool isTouchedBomb = false;

	this->scorePosY1 = 0;
	this->scorePosY2 = 0;

	int i = 0;
	//log("spriteBG1:count = %d", this->spriteBG1->getChildrenCount());
	auto& children = spriteBG->getChildren();
	for( auto it=std::begin(children); it != std::end(children); ++it)
	{
		i++;
		
		Sprite* egg = static_cast<Sprite*>(*it);
		
		int eggTag = egg->getTag();

		if ((eggTag >= GameManager::bombEgg_Tag)
			&& (egg->getScale() > 0.95f))
		{
			if ( this->checkCollision(spriteBG, egg, pigPos))				
			{
				if (eggTag == GameManager::bombEgg_Tag)
				{
					egg->runAction(FadeOut::create(0.15f));
					isTouchedBomb = true;

				} else if (eggTag >= GameManager::stoneEgg_Tag)
				{
					//--- Pig eats egg ---//
					this->pigEatsEgg(spriteBG, egg, pigPos);

					// Play sound
					if (GameManager::soundOn)
					{
						std::string tmpName = GameManager::getGoldSoundFileNameByTag(eggTag);
						SimpleAudioEngine::getInstance()->playEffect(tmpName.c_str(), false, 1, 0, 1);
					}

					//Call touched-on-gold effect
					this->touchedOnGoldEffect(ccp(pigPos.x, pigPos.y + this->pigSizeHeight*0.5));

					// --- update progress-bar
					this->updateScore(eggTag);

				}

				//break;
			}
		}
	}

	return isTouchedBomb;
}

bool Playing::checkCollision(CCSprite * parentBG, CCSprite * egg, Point pigPos)
{
  	Point posEgg = egg->getPosition();
	Point pos = parentBG->convertToWorldSpace(posEgg);

	bool chkResult = false;

	float distanceFromPigToEgg = ccpDistance(pigPos, pos);

	if (distanceFromPigToEgg < this->minCollisionDistance*0.45)
	{
		chkResult = true;
		
		// Show Value/Price (in USD) of gold
		int eggTag = egg->getTag();
		if (eggTag >= GameManager::stoneEgg_Tag) {

			std::string tmpName = GameManager::getUSDFileNameByTag(eggTag);
			auto spriteScore = Sprite::create(tmpName);
			spriteScore->setOpacity(0.0f);

			float addPosY = 0;

			if (this->scorePosY1 > 0)
			{
				addPosY = spriteScore->getContentSize().height*0.75f;
			} else if (this->scorePosY2 > 0)
			{
				//if (abs(posEgg.y - this->scorePosY2) <= spriteScore->getContentSize().height*0.55f)
				addPosY = spriteScore->getContentSize().height*(-0.75f);
			}

			float newPosY = pos.y - this->eggSizeHeight*0.5f + addPosY;
			spriteScore->setPosition(pos.x, newPosY);

			//parentBG->  gameObj
			this->addChild(spriteScore, 16, eggTag*(-1));
			
			auto removeThisScore = CCCallFuncN::create( this, callfuncN_selector(Playing::spriteScoreMoveFinished));
			auto fadeInOutRemove = Sequence::create(FadeIn::create(0.15f), CCDelayTime::create(0.5f), FadeOut::create(0.45f), CCDelayTime::create(0.25f), removeThisScore, NULL);

			CCMoveTo *moveTo = CCMoveTo::create(1.15f, ccp(pos.x, newPosY - this->eggSizeHeight*3));
	
			//Run both these actions simultaneously
			CCSpawn *spawn = CCSpawn::create(moveTo, fadeInOutRemove, NULL);

			spriteScore->runAction(spawn);

			if (this->scorePosY1 == 0)
				this->scorePosY1 = posEgg.y;
			else if (this->scorePosY2 == 0)
				this->scorePosY2 = posEgg.y;

			CCLOG("this->scorePosY1 = %d, this->scorePosY2 = %d",this->scorePosY1, this->scorePosY2);
		}
	}

	return chkResult;
}

void Playing::spriteScoreMoveFinished(CCNode* sender)
{
  CCSprite *sprite = (CCSprite *)sender;
  this->removeChild(sprite, true);
}


void Playing::pigEatsEgg(CCSprite * parentBG, CCSprite * egg, Point pigPos)
{
	egg->setScale(0.9f);

	Point posEgg = egg->getPosition();
	Point pos = parentBG->convertToWorldSpace(posEgg);

	float addPosX = pigPos.x - pos.x;
	float addPosY = pigPos.y - pos.y;

	Point toPos = ccp(posEgg.x + addPosX, posEgg.y + addPosY);

	CCMoveTo *moveTo = CCMoveTo::create(0.15f, toPos);
	CCScaleTo *scaleTo = CCScaleTo::create(0.15f, 0.0f);
	//Run both these actions simultaneously
	CCSpawn *spawn = CCSpawn::create(moveTo, scaleTo, NULL);
	egg->runAction(spawn);
}

void Playing::updateScore(int eggScore)
{
	//this->gotMoney = this->gotMoney + eggScore;
	GameManager::playingScore = GameManager::playingScore + eggScore;

	//this->currentLevel = 1;
	if (GameManager::playingScore >= this->targetScoreForCurrLevel)
	{
		GameManager::playingLevel++;

		//Playing Level
		char textLevel[256];
		sprintf(textLevel,"%d", GameManager::playingLevel);
		this->labelPlayingLevel->setString(textLevel);

		this->targetScoreForCurrLevel = GameManager::calculateLevelTarget(GameManager::playingLevel);
		this->topProgressBar->setPercentage((GameManager::playingScore/(this->targetScoreForCurrLevel))*100);

		//Level Up...
		this->spriteLevelUp->runAction(Sequence::create(FadeIn::create(0.25f), CCDelayTime::create(0.75f), FadeOut::create(0.45f),  NULL));

		// Play sound
		if (GameManager::soundOn)
			SimpleAudioEngine::getInstance()->playEffect(LEVEL_UP_QUICK_FILE, false, 1, 0, 1);

	} else {
		if (this->topProgressBar->getPercentage() < 100.0f)
			this->topProgressBar->setPercentage((GameManager::playingScore/(this->targetScoreForCurrLevel))*100);
	}

	//Score/money
	char textScore[256];
	sprintf(textScore,"$ %d", GameManager::playingScore);
	this->labelPlayingScores->setString(textScore);


	//For AdBuddiz - Giftiz
	if ((!this->missionCompleted) && (GameManager::playingScore >= 7500))
	{
		this->missionCompleted = true;
		CCUserDefault::getInstance()->setBoolForKey("MissionCompleted", this->missionCompleted);

		// Call missionCompleted
//		Application::missionCompleted();
	}
}

void Playing::checkingCollision(float delta)
{
	//this->unscheduleUpdate();
	//this->unschedule(schedule_selector(Playing::updatePigRunning));

	//check collision of Pig with eggs
	if ((!this->isJumpingForward) && (!this->isCheckingCollision))
	{
		this->isCheckingCollision = true;
		bool isCollisionWithBomb = false;

		Point pigPos = this->spritePig->getPosition();

		if (checkCollisionWithEggs(this->spriteBG1, pigPos))
		{
			isCollisionWithBomb = true;
		} else if (checkCollisionWithEggs(this->spriteBG2, pigPos))
		{
			isCollisionWithBomb = true;
		} else if (checkCollisionWithEggs(this->spriteBG3, pigPos))
		{
			isCollisionWithBomb = true;
		}

		if (isCollisionWithBomb)
		{
			CCLOG("checkingCollision: isCollisionWithBomb = true");
			this->unschedule(schedule_selector(Playing::checkingCollision));

			this->gameOver(pigPos);
		}

		this->isCheckingCollision = false;
	}

	//this->scheduleUpdate();
	//this->schedule(schedule_selector(Playing::updatePigRunning), 0.04f);
}

void Playing::goToLeft()
{
	this->isMoved = true;

	if (this->gravityX < 0)
	{
		if (this->gravityX >= -3.25 * this->screenRatio)
			this->gravityX = this->gravityX - 1.25f * this->screenRatio;
	} else {
		this->gravityX = -1.5f * this->screenRatio;
	}

	this->spritePig->setRotation(1.2f*this->gravityX);
}

void Playing::goToRight()
{
	this->isMoved = true;

	if (this->gravityX > 0)
	{
		if (this->gravityX <= 3.25 * this->screenRatio)
			this->gravityX = this->gravityX + 1.25f * this->screenRatio;
	} else {
		this->gravityX = 1.5f * this->screenRatio;
	}
	this->spritePig->setRotation(1.2f*this->gravityX);
}
