#ifndef __STARTUP_SCENE_H__
#define __STARTUP_SCENE_H__

#include "cocos2d.h"
USING_NS_CC;

class Startup : public cocos2d::Layer
{
private:
	void goToHomeScene(float dt);

public:
	
	Size screenSize;
	//Size visibleSize;
	Point centerPoint;

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    
    // implement the "static create()" method manually
    CREATE_FUNC(Startup);
};

#endif // __STARTUP_SCENE_H__
