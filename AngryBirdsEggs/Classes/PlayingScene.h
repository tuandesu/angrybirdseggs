#ifndef __PLAYING_SCENE_H__
#define __PLAYING_SCENE_H__

#include "cocos2d.h"
USING_NS_CC;

class Playing : public cocos2d::Layer
{
private:
	Size screenSize;
	float screenRatio;
	Point centerPoint;
	ProgressTimer* topProgressBar;
	float gotMoney;
	float nestWidth;
	float ctrlHeight;
	bool isMoved;

	//int currentLevel;
	float targetScoreForCurrLevel;

	bool isCheckingCollision;

	float oneUnitX;
	float oneUnitY;
	int eggSizeWidth;
	int eggSizeHeight;
	int pigSizeWidth;
	int pigSizeHeight;

	float minCollisionDistance;

	Menu *menuGo;
	Menu *menuPigCtrl;
	
	bool missionCompleted;

	Point beginPos;
	//Point endPos;

	std::string themeBgFile;

	CCLayerColor* colorLayer;
	CCNode* parentBG;
	CCSprite* spriteBG1;
	CCSprite* spriteBG2;
	CCSprite* spriteBG3;

	Label* labelPlayingLevel;
	Label* labelPlayingScores;

	Sprite *spritePig;
	Sprite *spriteLevelUp;

	bool isGameFinished;

	float scorePosY1;
	float scorePosY2;

	struct timeval startGameAt;
	
	
	struct timeval lastUpdate;
	//float currentTimeSpeed;
	//float slowestSpeed;
	float slowestSpeedStep;

	bool isJumpingForward;


	float gravityY;
	float gravityX;

	void startGame();
		
	void initGameItems();
	void initGameObjForOneBG(Sprite* spriteBG, int oneUnitX, int oneUnitY, bool isInitial);
	void initPig(int oneUnitY);
	void pigRunning();

	void addGoButton();
	void addCtrlButtons();

	// a selector callback: Rush
    void menuGoCallback(cocos2d::Ref* pSender);

    void menuLeftCallback(cocos2d::Ref* pSender);
    void menuRightCallback(cocos2d::Ref* pSender);

    void goToLeft();
    void goToRight();

    void slideToLeftRight(Point touchPoint);
    void checkAndJumpForward(Point endedPoint);
    void jumpForwardFinished();

    bool checkCollisionWithEggs(Sprite* spriteBG, Point pigPos);
    bool checkCollision(CCSprite * parentBG, CCSprite * egg, Point pigPos);
	void spriteScoreMoveFinished(CCNode* sender);
    void pigEatsEgg(CCSprite * parentBG, CCSprite * egg, Point pigPos);
    void updateScore(int eggScore);
	void checkingCollision(float delta);

	void touchedOnGoldEffect(Point pigPos);

	void update(float delta);
	//void updatePigRunning(float delta);
	void updateGameScolling(float dt);
	void updatePigSpeed(float delta);
		
	void gameOver(Point bombPos);
	//void showGameOver(float dt);
	void gameFinish();
	void goToResultScene(float dt);

	//void changeScrollingSpeed(bool isIncreaseSpeed);

	//void changeMinScollingSpeed(float dt);


public:

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callback: Classic
    void menuClassicCallback(cocos2d::Ref* pSender);

	// a selector callback: Zen
    void menuZenCallback(cocos2d::Ref* pSender);

	// a selector callback: Rush
    void menuRushCallback(cocos2d::Ref* pSender);

	// a selector callback: Leaderboard
    void menuLeaderboardCallback(cocos2d::Ref* pSender);

	// a selector callback: More
    void menuMoreCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(Playing);


	virtual bool onTouchBegan(Touch* touch, Event* event);
	virtual void onTouchMoved(Touch* touch, Event* event);
	virtual void onTouchEnded(Touch* touch, Event* event);

	virtual void onEnter();
};

#endif // __Playing_SCENE_H__
