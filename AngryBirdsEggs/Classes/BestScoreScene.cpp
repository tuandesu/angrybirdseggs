#include "BestScoreScene.h"
#include "Common/GameManager.h"
#include "Common/ResourceNames.h"
#include "HomeScene.h"

USING_NS_CC;
using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

Scene* BestScore::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = BestScore::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool BestScore::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->screenSize = Director::getInstance()->getOpenGLView()->getFrameSize(); 
	this->centerPoint = ccp(screenSize.width/2, screenSize.height/2);
	this->screenRatio = this->screenSize.width/320;

    /////////////////////////////
    
	// ----- add Backgound
    auto spriteBG = Sprite::create(s_bg_result);

    // position the sprite on the center of the screen
    spriteBG->setPosition(this->centerPoint);

    // add the sprite as a child to this layer
    this->addChild(spriteBG, 0, 0);
	
	
    auto spriteLogo = Sprite::create(s_angry_birds_eggs);

    // position the sprite on the center of the screen
    spriteLogo->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height - spriteLogo->getContentSize().height*1.25));

    // add the sprite as a child to this layer
    this->addChild(spriteLogo, 1, 1);

	
	// ----- add a "Share"
    auto shareItem = MenuItemImage::create(
                                           s_btn_share,
                                           s_btn_share_tapped,
                                           CC_CALLBACK_1(BestScore::menuShareCallback, this));

	shareItem->setPosition(this->screenSize.width - shareItem->getContentSize().width*0.55f, shareItem->getContentSize().height*0.55f);

	// ----- add a "Home" 
    auto homeItem = MenuItemImage::create(
                                           s_btn_home,
                                           s_btn_home_tapped,
                                           CC_CALLBACK_1(BestScore::menuHomeCallback, this));
    	
	homeItem->setPosition(homeItem->getContentSize().width*0.55f, homeItem->getContentSize().height*0.55f);
	
    // create menu, it's an autorelease object
    auto menu = Menu::create(homeItem, shareItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 2, 2);

	// Show my best result
	this->showMyBestResult();

    return true;
}

	
void BestScore::showMyBestResult()
{

	CCUserDefault::getInstance()->setIntegerForKey("BestResult_Level", GameManager::bestResult_Level);
	CCUserDefault::getInstance()->setIntegerForKey("BestResult_Score", GameManager::bestResult_Score);

	auto spriteMyBest = Sprite::create(s_my_best);
    spriteMyBest->setPosition(this->centerPoint.x, this->centerPoint.y + spriteMyBest->getContentSize().height*2.25);
    this->addChild(spriteMyBest, 3, 3);

	auto spriteStarBanner = Sprite::create(s_star_banner);
    spriteStarBanner->setPosition(this->centerPoint.x, this->centerPoint.y - spriteStarBanner->getContentSize().height*0.1);
    this->addChild(spriteStarBanner, 4, 4);

	// Level
	auto labelNewLevel_L = Label::createWithTTF("L", FONT_FEAST_OF_FLESH_BB, 36*this->screenRatio);
	labelNewLevel_L->setPosition(this->centerPoint.x - this->screenSize.width*0.11,
								this->centerPoint.y - labelNewLevel_L->getContentSize().height*1.25);
	this->addChild(labelNewLevel_L, 5, 5);

	auto labelNewLevel_evel = Label::createWithTTF("evel", FONT_FEAST_OF_FLESH_BB, 24*this->screenRatio);
	labelNewLevel_evel->setPosition(labelNewLevel_L->getPositionX() + labelNewLevel_L->getContentSize().width*0.5  + labelNewLevel_evel->getContentSize().width*0.5,
								this->centerPoint.y - labelNewLevel_L->getContentSize().height*1.35);
	this->addChild(labelNewLevel_evel, 5, 5);

	char textLevel[256];
	sprintf(textLevel,"%d", GameManager::bestResult_Level);
	auto labelNewLevel = Label::createWithTTF(textLevel, FONT_FEAST_OF_FLESH_BB, 36*this->screenRatio);
	labelNewLevel->setPosition(this->centerPoint.x + this->screenSize.width*0.11,
								this->centerPoint.y - labelNewLevel_L->getContentSize().height*1.25);
	this->addChild(labelNewLevel, 6, 6);

	// Score
	char textScore[256];
	sprintf(textScore,"$ %d", GameManager::bestResult_Score);
	auto labelNewScore = Label::createWithTTF(textScore, FONT_FEAST_OF_FLESH_BB, 36*this->screenRatio);
	labelNewScore->setPosition(this->centerPoint.x,
								labelNewLevel->getPositionY() - labelNewLevel_L->getContentSize().height*1.15);
	labelNewScore->setColor(ccc3(255,210,2));
	this->addChild(labelNewScore, 7, 7);

	//Stars

	auto spriteStar1 = Sprite::create(s_star1);
    spriteStar1->setPosition(this->centerPoint.x - spriteStar1->getContentSize().width*0.83, this->centerPoint.y + spriteStarBanner->getContentSize().height*0.25);
	this->addChild(spriteStar1, 8, 8);

	auto spriteStar2 = Sprite::create(s_star2);
    spriteStar2->setPosition(this->centerPoint.x, this->centerPoint.y + spriteStarBanner->getContentSize().height*0.35);
	this->addChild(spriteStar2, 9, 9);

	auto spriteStar3 = Sprite::create(s_star3);
    spriteStar3->setPosition(this->centerPoint.x + spriteStar1->getContentSize().width*0.83, this->centerPoint.y + spriteStarBanner->getContentSize().height*0.25);
	this->addChild(spriteStar3, 10, 10);

}


void BestScore::menuShareCallback(Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}


    this->capture();
    
}

void BestScore::menuHomeCallback(Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}

	CCTransitionFade* transition = CCTransitionFade::create(1, Home::createScene(), ccBLACK);
    CCDirector::getInstance()->replaceScene(transition);
}



void BestScore::afterCaptured(bool succeed, const std::string& outputFile)
{
    if (succeed)
    {
        // show screenshot
        auto sp = Sprite::create(outputFile);
        this->addChild(sp, 111, 111);
        sp->setPosition(this->centerPoint);
        //sp->setScale(0.25);
		
		ActionInterval *scaleTo = ScaleTo::create(0.1f, 1.15f);
		ActionInterval *scaleToBack = ScaleTo::create(0.25, 0.0f);
		Sequence *actSequence = (Sequence*)Sequence::create(scaleTo, CCDelayTime::create(0.1f), scaleToBack,  NULL); //CCDelayTime::create(1.0f), fadeOut,

		sp->runAction(actSequence);

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

		Application::getInstance()->setScrShotPathToJava(outputFile.c_str());
    
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    
		// TODO 
    
#endif

		
    }
    else
    {
        log("Capture screen failed.");
    }
}

void BestScore::capture()
{
    utils::captureScreen(CC_CALLBACK_2(BestScore::afterCaptured, this), SCREENSHOT_FILE_PATH); //"/sdcard/CaptureScreen.jpg" .png
}