#ifndef __BESTSCORE_SCENE_H__
#define __BESTSCORE_SCENE_H__

#include "cocos2d.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	#define SCREENSHOT_FILE_PATH         "/sdcard/AB_Eggs_CaptureScreen.jpg"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	#define SCREENSHOT_FILE_PATH         "AB_Eggs_CaptureScreen.jpg"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
	#define SCREENSHOT_FILE_PATH         "AB_Eggs_CaptureScreen.jpg"
#else
	#define SCREENSHOT_FILE_PATH         "AB_Eggs_CaptureScreen.jpg"//(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#endif

USING_NS_CC;

class BestScore : public cocos2d::Layer
{
private:
	Size screenSize;
	//Size visibleSize;
	Point centerPoint;
	float screenRatio;
	
	void showMyBestResult();
	
	void capture();
	void afterCaptured(bool succeed, const std::string& outputFile);

public:	

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
	// a selector callback: Zen
    void menuShareCallback(cocos2d::Ref* pSender);

    // a selector callback: Classic
    void menuHomeCallback(cocos2d::Ref* pSender);

    
    // implement the "static create()" method manually
    CREATE_FUNC(BestScore);
};

#endif // __BESTSCORE_SCENE_H__
