#ifndef __HOME_SCENE_H__
#define __HOME_SCENE_H__

#include "cocos2d.h"
USING_NS_CC;

class Home : public cocos2d::Layer
{
private:
	Size screenSize;
	Point centerPoint;
	float screenRatio;

	Menu* moreMenu;
	Menu* optionMenu;

	void createMoreMenu(MenuItemImage* moreItem);
	void createOptionMn(MenuItemImage* optionItem);
	
	CCMenuItemToggle* optSoundItem;
	CCMenuItemToggle* bgMusicItem;

public:

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    
	// a selector callback: Rush
    void menuPlayCallback(cocos2d::Ref* pSender);

	// a selector callback: More
    void menuMoreCallback(cocos2d::Ref* pSender);	

	// a selector callback: Options
    void menuOptionsCallback(cocos2d::Ref* pSender);

	void menuAboutCallback(cocos2d::Ref* pSender);

	// a selector callback: Best Score
    void menuBScoresCallback(cocos2d::Ref* pSender);

	void menuSoundCallback(cocos2d::Ref* pSender);

	// a selector callback: BG Music
    void menuBgMusicCallback(cocos2d::Ref* pSender);

    // a selector callback: AdBuddiz - Giftiz
    void menuGiftCallback(cocos2d::Ref* pSender);

	void menuNothingCallback(Ref* pSender);

    // implement the "static create()" method manually
    CREATE_FUNC(Home);
};

#endif // __HOME_SCENE_H__
