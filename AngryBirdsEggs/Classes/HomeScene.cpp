#include "HomeScene.h"
#include "cocos2d.h"
#include "PlayingScene.h"
#include "SimpleAudioEngine.h"
#include "Common/GameManager.h"
#include "Common/ResourceNames.h"
#include "AboutScene.h"
#include "BestScoreScene.h"



USING_NS_CC;

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;


Scene* Home::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = Home::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Home::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->screenSize = Director::getInstance()->getOpenGLView()->getFrameSize(); 
	this->centerPoint = ccp(screenSize.width/2, screenSize.height/2);
	this->screenRatio = this->screenSize.width/320;

    /////////////////////////////
    
	// ----- add Backgound
    auto spriteBG = Sprite::create(s_bg_home);

    // position the sprite on the center of the screen
    spriteBG->setPosition(this->centerPoint);

    // add the sprite as a child to this layer
    this->addChild(spriteBG, 0);

	// ----- add "Logo"
    auto spriteLogo = Sprite::create(s_angry_birds_eggs);

    // position the sprite on the center of the screen
    spriteLogo->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height - spriteLogo->getContentSize().height*1.25));

    // add the sprite as a child to this layer
    this->addChild(spriteLogo, 1);


    // ----- add a "Play" 
    auto playItem = MenuItemImage::create(
                                           s_btn_play,
                                           s_btn_play_tapped,
                                           CC_CALLBACK_1(Home::menuPlayCallback, this));
    	
	playItem->setPosition(Vec2(this->centerPoint.x,
                                this->centerPoint.y - playItem->getContentSize().height*1.15));


	// ----- add a "options" 
    auto optionsItem = MenuItemImage::create(
                                           s_btn_option,
                                           s_btn_option_tapped,
                                           CC_CALLBACK_1(Home::menuOptionsCallback, this));
    	
	optionsItem->setPosition(Vec2(origin.x + optionsItem->getContentSize().width*0.6,
                                origin.y + optionsItem->getContentSize().height*0.6));

	// ----- add a "gift"
//	auto giftItem = MenuItemImage::create(
//										   s_btn_gift,
//										   s_btn_gift_tapped,
//										   CC_CALLBACK_1(Home::menuGiftCallback, this));

//	giftItem->setPosition(Vec2(this->centerPoint.x,
//								origin.y + optionsItem->getContentSize().height*0.6));
	//

	// ----- add a "More..." 
    auto moreItem = MenuItemImage::create(
                                           s_btn_more,
                                           s_btn_more_tapped,
                                           CC_CALLBACK_1(Home::menuMoreCallback, this));
    	
	moreItem->setPosition(Vec2(origin.x + visibleSize.width - moreItem->getContentSize().width*0.6,
                                origin.y + moreItem->getContentSize().height*0.6));

	
    // create menu, it's an autorelease object
    auto menu = Menu::create(playItem, optionsItem, moreItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 10, 10);

 
	// Create more-menu
	this->createMoreMenu(moreItem);

	// Create billionaires board
	this->createOptionMn(optionsItem);
	
	// Play background music
	if ((GameManager::backgroundMusicOn) && (!SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()))
		SimpleAudioEngine::getInstance()->playBackgroundMusic(BG_MUSIC_FILE, true);

    return true;
}

void Home::createMoreMenu(MenuItemImage* moreItem )
{
	//more_menu_bg	
    auto moreMenuBgItem = MenuItemImage::create(
                                           s_mn_bg,
                                           s_mn_bg,
                                           CC_CALLBACK_1(Home::menuNothingCallback, this));
	moreMenuBgItem->setPosition(moreItem->getPositionX(),
                                moreItem->getPositionY() + moreMenuBgItem->getContentSize().height*0.5);

	//about	
    auto aboutItem = MenuItemImage::create(
                                           s_btn_about,
                                           s_btn_about_tapped,
                                           CC_CALLBACK_1(Home::menuAboutCallback, this));
    
	aboutItem->setPosition(moreItem->getPositionX(),
                                moreItem->getPositionY() + aboutItem->getContentSize().height*0.85);
	
	//best scores	
    auto bscoreItem = MenuItemImage::create(
                                           s_btn_best_score,
                                           s_btn_best_score_tapped,
                                           CC_CALLBACK_1(Home::menuBScoresCallback, this));
    
	bscoreItem->setPosition(moreItem->getPositionX(),
                                aboutItem->getPositionY() + bscoreItem->getContentSize().height*0.85);
		
	// create menu, it's an autorelease object
	this->moreMenu = Menu::create(moreMenuBgItem, aboutItem, bscoreItem, NULL);

	
    this->moreMenu->setPosition(Vec2::ZERO);
    this->addChild(this->moreMenu, 3, 3);
	this->moreMenu->setEnabled(false);
	this->moreMenu->setOpacity(0.0f);
}

void Home::createOptionMn(MenuItemImage* optionItem)
{
	//more_menu_bg	
    auto moreMenuBgItem = MenuItemImage::create(
                                           s_mn_bg,
                                           s_mn_bg,
                                           CC_CALLBACK_1(Home::menuNothingCallback, this));
	moreMenuBgItem->setPosition(optionItem->getPositionX(),
                                optionItem->getPositionY() + moreMenuBgItem->getContentSize().height*0.5);

	// Game Sounds
	CCMenuItemSprite* optSoundSprite = CCMenuItemSprite::create(CCSprite::create(s_btn_sound_on), NULL);
	CCMenuItemSprite* optSoundTappedSprite = CCMenuItemSprite::create(CCSprite::create(s_btn_sound_off), NULL);
	
	this->optSoundItem = CCMenuItemToggle::createWithTarget( this,menu_selector(Home::menuSoundCallback), optSoundSprite, optSoundTappedSprite,NULL );
		
	this->optSoundItem->setPosition(optionItem->getPositionX(), optionItem->getPositionY() + optSoundSprite->getContentSize().height*0.975f);
	if (GameManager::soundOn) 		
		this->optSoundItem->setSelectedIndex(0);
	else
		this->optSoundItem->setSelectedIndex(1);

	// BG Music
	CCMenuItemSprite* bgMusicSprite = CCMenuItemSprite::create(CCSprite::create(s_btn_bg_music_on), NULL);
	CCMenuItemSprite* bgMusicTappedSprite = CCMenuItemSprite::create(CCSprite::create(s_btn_bg_music_off), NULL);
	
	this->bgMusicItem = CCMenuItemToggle::createWithTarget( this,menu_selector(Home::menuBgMusicCallback), bgMusicSprite, bgMusicTappedSprite,NULL );
		
	this->bgMusicItem->setPosition(optSoundItem->getPositionX(), optSoundItem->getPositionY() + bgMusicSprite->getContentSize().height*0.975f);

	if (GameManager::backgroundMusicOn) 
		this->bgMusicItem->setSelectedIndex(0);
	else
		this->bgMusicItem->setSelectedIndex(1);

	// create menu, it's an autorelease object
    this->optionMenu = Menu::create(moreMenuBgItem, this->optSoundItem, this->bgMusicItem, NULL);
    this->optionMenu->setPosition(Vec2::ZERO);
    this->addChild(this->optionMenu, 5, 5);
	
	this->optionMenu->setEnabled(false);
	this->optionMenu->setOpacity(0.0f);
	this->optSoundItem->setOpacity(0.0f);
	this->bgMusicItem->setOpacity(0.0f);
	this->optionMenu->setZOrder(-5);
}

void Home::menuPlayCallback(Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		//string soundFileName = "sounds/touch_8.mp3";
		//SimpleAudioEngine::getInstance()->playEffect(const_cast<char*>(soundFileName.c_str())) ;

		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}

	if (GameManager::backgroundMusicOn) 
	{
		SimpleAudioEngine::getInstance()->stopBackgroundMusic();
		//SimpleAudioEngine::getInstance()->playBackgroundMusic(MUSIC_FILE, true);
	}
	CCTransitionFade* transition = CCTransitionFade::create(1, Playing::createScene(), ccBLACK);
    CCDirector::getInstance()->replaceScene(transition);
}

void Home::menuOptionsCallback(Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}

	if (!this->optionMenu->isEnabled())
	{		
		//this->colorLayer->setVisible(true);
		this->optionMenu->setEnabled(true);
		this->optionMenu->runAction(FadeIn::create(0.15f));
		this->optSoundItem->runAction(FadeIn::create(0.15f));
		this->bgMusicItem->runAction(FadeIn::create(0.15f));
		this->optionMenu->setZOrder(5);

		this->moreMenu->setEnabled(false);
		this->moreMenu->runAction(FadeOut::create(0.15f));	
	} else {
		this->optionMenu->setEnabled(false);
		this->optionMenu->runAction(FadeOut::create(0.15f));	
		this->optSoundItem->runAction(FadeOut::create(0.15f));
		this->bgMusicItem->runAction(FadeOut::create(0.15f));
		this->optionMenu->setZOrder(-5);
	}

}

void Home::menuMoreCallback(Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}
	
	if (!this->moreMenu->isEnabled())
	{
		//this->colorLayer->setVisible(true);
		this->moreMenu->setEnabled(true);
		this->moreMenu->runAction(FadeIn::create(0.15f));

		this->optionMenu->setEnabled(false);
		this->optionMenu->runAction(FadeOut::create(0.15f));	
		this->optSoundItem->runAction(FadeOut::create(0.15f));
		this->bgMusicItem->runAction(FadeOut::create(0.15f));
		this->optionMenu->setZOrder(-5);
	} else {
		this->moreMenu->setEnabled(false);
		this->moreMenu->runAction(FadeOut::create(0.15f));	
	}
}


void Home::menuNothingCallback(Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		//SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}
}

// a selector callback: About
void Home::menuAboutCallback(cocos2d::Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}

	CCTransitionFade* transition = CCTransitionFade::create(1, About::createScene(), ccBLACK);
    CCDirector::getInstance()->replaceScene(transition);
}

// a selector callback: Options
void Home::menuSoundCallback(cocos2d::Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}

	if (GameManager::soundOn)
		GameManager::soundOn = false;
	else
		GameManager::soundOn = true;
	
	CCUserDefault::getInstance()->setBoolForKey("soundOn", GameManager::soundOn); 
}

void Home::menuBgMusicCallback(Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}
	

	if (GameManager::backgroundMusicOn)
	{
		GameManager::backgroundMusicOn = false;
		if (SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
			SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
	}
	else
	{
		GameManager::backgroundMusicOn = true;
		SimpleAudioEngine::getInstance()->playBackgroundMusic(BG_MUSIC_FILE, true);
	}
	
	CCUserDefault::getInstance()->setBoolForKey("backgroundMusicOn", GameManager::backgroundMusicOn); 
		
}

// a selector callback: Best Score
void Home::menuBScoresCallback(cocos2d::Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}
	
	CCTransitionFade* transition = CCTransitionFade::create(1, BestScore::createScene(), ccBLACK);
    CCDirector::getInstance()->replaceScene(transition);
}

void Home::menuGiftCallback(cocos2d::Ref* pSender)
{
	if (GameManager::soundOn)
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}

//	Application::giftButtonClicked();
}



