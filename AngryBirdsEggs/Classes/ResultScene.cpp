#include "ResultScene.h"
#include "HomeScene.h"
#include "PlayingScene.h"
#include "Common/GameManager.h" 
#include "Common/ResourceNames.h"
#include "Object-C-Inteface.h"

//#include "platform/android/jni/JniHelper.h"
//#include <android/log.h>
//#include <jni.h>


USING_NS_CC;

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;


Scene* PlayingResult::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PlayingResult::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayingResult::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->screenSize = Director::getInstance()->getOpenGLView()->getFrameSize(); 
	this->centerPoint = ccp(screenSize.width/2, screenSize.height/2);
	this->screenRatio = this->screenSize.width/320;
    /////////////////////////////

	this->pinStarCounter = 0;
	this->pressedReplayCounter = 0;
	//this->adsIsShowing = false;
	GameManager::isFailedGame = false;
    
	//std::string txtBg = s_bg_result;

	//=================================//
	GameManager::playingCounter = CCUserDefault::getInstance()->getIntegerForKey("PlayingCounter", 0);
	GameManager::playingCounter++;
	CCUserDefault::getInstance()->setIntegerForKey("PlayingCounter", GameManager::playingCounter);

	if ((GameManager::playingCounter == 1) && (GameManager::playingLevel == 1))
	{
		//txtBg = s_bg_result_failed;
		GameManager::isFailedGame = true;

		GameManager::bestResult_Score = GameManager::playingScore;
		GameManager::bestResult_Level = GameManager::playingLevel;
		CCUserDefault::getInstance()->setIntegerForKey("BestResult_Score", GameManager::bestResult_Score);
		CCUserDefault::getInstance()->setIntegerForKey("BestResult_Level", GameManager::bestResult_Level);
	} else {
		if (GameManager::playingScore > GameManager::bestResult_Score)
		{
			GameManager::bestResult_Score = GameManager::playingScore;
			GameManager::bestResult_Level = GameManager::playingLevel;
			CCUserDefault::getInstance()->setIntegerForKey("BestResult_Score", GameManager::bestResult_Score);
			CCUserDefault::getInstance()->setIntegerForKey("BestResult_Level", GameManager::bestResult_Level);
		} else {
			//txtBg = s_bg_result_failed;
			GameManager::isFailedGame = true;
		}
	}

	// ----- add Backgound
    auto spriteBG = Sprite::create(s_bg_result); //txtBg.c_str()

    // position the sprite on the center of the screen
    spriteBG->setPosition(this->centerPoint);

    // add the sprite as a child to this layer
    this->addChild(spriteBG, 0, 0);

	
    auto spriteLogo = Sprite::create(s_angry_birds_eggs);

    // position the sprite on the center of the screen
    spriteLogo->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height - spriteLogo->getContentSize().height*1.25));

    // add the sprite as a child to this layer
    this->addChild(spriteLogo, 1, 1);

	// ----- add a "Replay" 
    auto replayItem = MenuItemImage::create(
                                           s_btn_replay,
                                           s_btn_replay_tapped,
                                           CC_CALLBACK_1(PlayingResult::menuReplayCallback, this));
    //origin.x + 
	replayItem->setPosition(this->centerPoint.x, replayItem->getContentSize().width*0.5f);	

	// ----- add a "Share"
    auto shareItem = MenuItemImage::create(
                                           s_btn_share,
                                           s_btn_share_tapped,
                                           CC_CALLBACK_1(PlayingResult::menuShareCallback, this));

	shareItem->setPosition(this->screenSize.width - shareItem->getContentSize().width*0.55f, replayItem->getPositionY());

	// ----- add a "Home" 
    auto homeItem = MenuItemImage::create(
                                           s_btn_home,
                                           s_btn_home_tapped,
                                           CC_CALLBACK_1(PlayingResult::menuHomeCallback, this));
    	
	homeItem->setPosition(homeItem->getContentSize().width*0.55f, replayItem->getPositionY());
	
    // create menu, it's an autorelease object
    auto menu = Menu::create(homeItem, shareItem, replayItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 2, 2);




	//=================================//
	if (GameManager::isFailedGame)
	{
		this->showFailedResult();
	} else {
		this->showNewBestResult();

//		bool missionCompleted = CCUserDefault::getInstance()->getBoolForKey("MissionCompleted", false);
//
//		if ((!missionCompleted) && (GameManager::bestResult_Level >= 3))
//		{
//			missionCompleted = true;
//			CCUserDefault::getInstance()->setBoolForKey("MissionCompleted", missionCompleted);
//
//			// Call missionCompleted
//			Application::missionCompleted();
//		}
	}



	//=================================//
	GameManager::isFailedGame = false;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	//Show Google AdMob
	//Application::showAd();

	//=================================//

	this->isRated = CCUserDefault::getInstance()->getBoolForKey("IsRatedThisGame", false);

	//Show rate this game
	this->schedule(schedule_selector(PlayingResult::showRateThisGame), 2.5f);
#else
	//TODO...
    
#endif // CC_PLATFOR_ANDROID
	
	
    return true;
}

void PlayingResult::createRateMn()
{
	//bg_black
	auto blackBgItem = MenuItemImage::create(
										   s_bg_black,
										   s_bg_black,
										   CC_CALLBACK_1(PlayingResult::menuBlackBgCallback, this));

	blackBgItem->setPosition(this->centerPoint);

	//bg
	auto dialogBgItem = MenuItemImage::create(
											s_dialog_bg,
											s_dialog_bg,
										   CC_CALLBACK_1(PlayingResult::menuBlackBgCallback, this));

	dialogBgItem->setPosition(this->centerPoint);

	//msg: Rate this game
	auto rareThisGameItem = MenuItemImage::create(
											s_rate_this_game,
											s_rate_this_game,
										   CC_CALLBACK_1(PlayingResult::menuBlackBgCallback, this));

	rareThisGameItem->setPosition(dialogBgItem->getPositionX(),
								dialogBgItem->getPositionY() + rareThisGameItem->getContentSize().height*2.0f);

	//Yes
	auto yesItem = MenuItemImage::create(
											s_btn_yes,
											s_btn_yes_tapped,
										   CC_CALLBACK_1(PlayingResult::menuYesCallback, this));

	yesItem->setPosition(dialogBgItem->getPositionX() - yesItem->getContentSize().width*0.75,
							dialogBgItem->getPositionY() - yesItem->getContentSize().height*0.75);

	//No
	auto noItem = MenuItemImage::create(
											s_btn_no,
											s_btn_no_tapped,
										   CC_CALLBACK_1(PlayingResult::menuNoCallback, this));

	noItem->setPosition(dialogBgItem->getPositionX() + noItem->getContentSize().width*0.75,
							dialogBgItem->getPositionY() - noItem->getContentSize().height*0.75);


	// create menu, it's an autorelease object
	this->rateMenu = Menu::create(blackBgItem, dialogBgItem, rareThisGameItem, yesItem, noItem, NULL);
	this->rateMenu->setPosition(Vec2::ZERO);
	this->addChild(this->rateMenu, 30, 30);
	//this->rateMenu->setEnabled(false);
	this->rateMenu->setOpacity(0.0f);
	this->rateMenu->runAction(FadeIn::create(0.25f));
}
	
void PlayingResult::menuBlackBgCallback(Ref* pSender)
{
	//this->hideRateMn();
}

// a selector callback: Yes
void PlayingResult::menuYesCallback(cocos2d::Ref* pSender)
{
	this->hideRateMn();

	// Call rate
//	Application::rateApp();

	this->isRated = true;
	CCUserDefault::getInstance()->setBoolForKey("IsRatedThisGame", this->isRated);
}

// a selector callback: No
void PlayingResult::menuNoCallback(cocos2d::Ref* pSender)
{
	this->hideRateMn();
}

void PlayingResult::hideRateMn()
{
	if (this->rateMenu->isEnabled())
	{
		this->rateMenu->setEnabled(false);
		this->rateMenu->runAction(FadeOut::create(0.25f));
	}
}

//Show Rate this game
void PlayingResult::showRateThisGame(float dt)
{
	this->unschedule(schedule_selector(PlayingResult::showRateThisGame));

//	if (Application::isNetworkAvailable())
//	{
//		if (GameManager::bestResult_Level >= 2)
//		{
//			bool rateNow = false;
//
//			if (this->isRated)
//			{
//				if ((GameManager::playingCounter == 100)
//						|| (GameManager::playingCounter == 200))
//					rateNow = true;
//			} else {
//				if (GameManager::playingCounter % 2 == 0)
//					rateNow = true;
//			}
//
//			if (rateNow)
//				//Create Rate Menu
//				this->createRateMn();
//		}
//	}
}

void PlayingResult::showNewBestResult()
{

	//Shows result
	auto spriteNewBest = Sprite::create(s_new_best);
    spriteNewBest->setPosition(this->centerPoint.x, this->centerPoint.y + spriteNewBest->getContentSize().height*2.25);
    this->addChild(spriteNewBest, 3, 3);

	auto spriteStarBanner = Sprite::create(s_star_banner);
    spriteStarBanner->setPosition(this->centerPoint.x, this->centerPoint.y - spriteStarBanner->getContentSize().height*0.1);
    this->addChild(spriteStarBanner, 4, 4);

	// Level
	auto labelNewLevel_L = Label::createWithTTF("L", FONT_FEAST_OF_FLESH_BB, 36*this->screenRatio);
	labelNewLevel_L->setPosition(this->centerPoint.x - this->screenSize.width*0.11,
								this->centerPoint.y - labelNewLevel_L->getContentSize().height*1.25);
	this->addChild(labelNewLevel_L, 5, 5);

	auto labelNewLevel_evel = Label::createWithTTF("evel", FONT_FEAST_OF_FLESH_BB, 24*this->screenRatio);
	labelNewLevel_evel->setPosition(labelNewLevel_L->getPositionX() + labelNewLevel_L->getContentSize().width*0.5  + labelNewLevel_evel->getContentSize().width*0.5,
								this->centerPoint.y - labelNewLevel_L->getContentSize().height*1.35);
	this->addChild(labelNewLevel_evel, 5, 5);

	char textLevel[256];
	sprintf(textLevel,"%d", GameManager::bestResult_Level);
	auto labelNewLevel = Label::createWithTTF(textLevel, FONT_FEAST_OF_FLESH_BB, 36*this->screenRatio);
	labelNewLevel->setPosition(this->centerPoint.x + this->screenSize.width*0.11,
								this->centerPoint.y - labelNewLevel_L->getContentSize().height*1.25);
	this->addChild(labelNewLevel, 6, 6);

	// Score
	char textScore[256];
	sprintf(textScore,"$ %d", GameManager::bestResult_Score);
	auto labelNewScore = Label::createWithTTF(textScore, FONT_FEAST_OF_FLESH_BB, 36*this->screenRatio);
	labelNewScore->setPosition(this->centerPoint.x,
								labelNewLevel->getPositionY() - labelNewLevel_L->getContentSize().height*1.15);
	labelNewScore->setColor(ccc3(255,210,2));
	this->addChild(labelNewScore, 7, 7);

	auto spriteStar1 = Sprite::create(s_star1);
    spriteStar1->setPosition(this->centerPoint.x - this->screenSize.width*0.7, this->centerPoint.y + this->screenSize.height*0.6);
	spriteStar1->setScale(3.0f);
	spriteStar1->setOpacity(0.0f);
    this->addChild(spriteStar1, 8, 8);

	auto spriteStar2 = Sprite::create(s_star2);
    spriteStar2->setPosition(this->centerPoint.x, this->centerPoint.y + this->screenSize.height*0.7);
	spriteStar2->setScale(3.0f);
	spriteStar2->setOpacity(0.0f);
    this->addChild(spriteStar2, 9, 9);

	auto spriteStar3 = Sprite::create(s_star3);
    spriteStar3->setPosition(this->centerPoint.x + this->screenSize.width*0.7, this->centerPoint.y + this->screenSize.height*0.6);
	spriteStar3->setScale(3.0f);
	spriteStar3->setOpacity(0.0f);
    this->addChild(spriteStar3, 10, 10);

	Point star1Pos = ccp(this->centerPoint.x - spriteStar1->getContentSize().width*0.83, this->centerPoint.y + spriteStarBanner->getContentSize().height*0.25);
	Point star2Pos = ccp(this->centerPoint.x, this->centerPoint.y + spriteStarBanner->getContentSize().height*0.35);
	Point star3Pos = ccp(this->centerPoint.x + spriteStar1->getContentSize().width*0.83, this->centerPoint.y + spriteStarBanner->getContentSize().height*0.25);

	CCSpawn *spawn1 = CCSpawn::create(CCMoveTo::create(0.35f, star1Pos), FadeIn::create(0.25f), CCScaleTo::create(0.35f, 1.0f), NULL);
	auto seqActions1 = Sequence::create(CCDelayTime::create(0.9f), spawn1, NULL);
	spriteStar1->runAction(seqActions1);

	CCSpawn *spawn2 = CCSpawn::create(CCMoveTo::create(0.35f, star2Pos), FadeIn::create(0.25f), CCScaleTo::create(0.35f, 1.0f), NULL);
	auto seqActions2 = Sequence::create(CCDelayTime::create(1.35f), spawn2, NULL);
	spriteStar2->runAction(seqActions2);

	CCSpawn *spawn3 = CCSpawn::create(CCMoveTo::create(0.35f, star3Pos), FadeIn::create(0.25f), CCScaleTo::create(0.35f, 1.0f), NULL);
	auto seqActions3 = Sequence::create(CCDelayTime::create(1.8f), spawn3, NULL);
	spriteStar3->runAction(seqActions3);


	this->schedule(schedule_selector(PlayingResult::pinStarSound), 1.25f);

	//Fireworks effect
	this->fireworksEffect();
	
	
}


void PlayingResult::pinStarSound(float dt)
{
	this->unschedule(schedule_selector(PlayingResult::pinStarSound));
	this->pinStarCounter++;

	if (this->pinStarCounter <= 3)
	{
		if (GameManager::soundOn)
		{
			SimpleAudioEngine::getInstance()->playEffect(PIN_STAR_FILE_01, false, 1, 0, 1);
		}
		this->schedule(schedule_selector(PlayingResult::pinStarSound), 0.45f);
	} else {
		//Rate this game
		//this->rateThisGame();
		//this->schedule(schedule_selector(PlayingResult::showRateThisGame), 0.1f);
	}

}



void PlayingResult::showFailedResult()
{
	auto spriteFailed = Sprite::create(s_failed);
    spriteFailed->setPosition(this->centerPoint.x, this->centerPoint.y + spriteFailed->getContentSize().height*2.0);
    this->addChild(spriteFailed, 3, 3);

	// Playing Level
	auto labelLevel_L = Label::createWithTTF("L", FONT_FEAST_OF_FLESH_BB, 30*this->screenRatio);
	labelLevel_L->setPosition(this->centerPoint.x - this->screenSize.width*0.11,
								this->centerPoint.y + labelLevel_L->getContentSize().height*1.15);
	this->addChild(labelLevel_L, 4, 4);

	auto labelLevel_evel = Label::createWithTTF("evel", FONT_FEAST_OF_FLESH_BB, 22*this->screenRatio);
	labelLevel_evel->setPosition(labelLevel_L->getPositionX() + labelLevel_L->getContentSize().width*0.5  + labelLevel_evel->getContentSize().width*0.5,
								this->centerPoint.y + labelLevel_L->getContentSize().height*1.05);
	this->addChild(labelLevel_evel, 5, 5);

	char textLevel[256];
	sprintf(textLevel,"%d", GameManager::playingLevel);
	auto labelLevel = Label::createWithTTF(textLevel, FONT_FEAST_OF_FLESH_BB, 30*this->screenRatio);
	labelLevel->setPosition(labelLevel_evel->getPositionX() + labelLevel_evel->getContentSize().width*1.15,
								this->centerPoint.y + labelLevel_L->getContentSize().height*1.15);
	this->addChild(labelLevel, 6, 6);

	// Playing Score
	char textScore[256];
	sprintf(textScore,"$ %d", GameManager::playingScore);
	auto labelScore = Label::createWithTTF(textScore, FONT_FEAST_OF_FLESH_BB, 33*this->screenRatio);
	labelScore->setPosition(this->centerPoint);
	labelScore->setColor(ccc3(255,210,2));
	this->addChild(labelScore, 7, 7);

	// Best Level
	auto labelBestLevel_B = Label::createWithTTF("B", FONT_FEAST_OF_FLESH_BB, 16*this->screenRatio);
	labelBestLevel_B->setPosition(this->centerPoint.x - this->screenSize.width*0.125,
								this->centerPoint.y - labelBestLevel_B->getContentSize().height*3.5);
	this->addChild(labelBestLevel_B, 8, 8);

	auto labelBestLevel_est = Label::createWithTTF("est:", FONT_FEAST_OF_FLESH_BB, 13*this->screenRatio);
	labelBestLevel_est->setPosition(labelBestLevel_B->getPositionX() + labelBestLevel_B->getContentSize().width*0.4  + labelBestLevel_est->getContentSize().width*0.5,
								this->centerPoint.y - labelBestLevel_B->getContentSize().height*3.6);
	this->addChild(labelBestLevel_est, 9, 9);

	auto labelBestLevel_L = Label::createWithTTF("L", FONT_FEAST_OF_FLESH_BB, 16*this->screenRatio);
	labelBestLevel_L->setPosition(labelBestLevel_est->getPositionX() + labelBestLevel_est->getContentSize().width*1.25,
								labelBestLevel_B->getPositionY());
	labelBestLevel_L->setAnchorPoint(Vec2(0.0f, 0.5f));
	this->addChild(labelBestLevel_L, 10, 10);

	auto labelBestLevel_evel = Label::createWithTTF("evel", FONT_FEAST_OF_FLESH_BB, 13*this->screenRatio);
	labelBestLevel_evel->setPosition(labelBestLevel_L->getPositionX() + labelBestLevel_L->getContentSize().width*0.9,
								labelBestLevel_est->getPositionY());
	labelBestLevel_evel->setAnchorPoint(Vec2(0.0f, 0.5f));
	this->addChild(labelBestLevel_evel, 11, 11);
		
	sprintf(textLevel,"%d", GameManager::bestResult_Level);
	auto labelBestLevel = Label::createWithTTF(textLevel, FONT_FEAST_OF_FLESH_BB, 16*this->screenRatio);
	labelBestLevel->setPosition(labelBestLevel_evel->getPositionX() + labelBestLevel_evel->getContentSize().width*1.25,
								labelBestLevel_B->getPositionY());
	labelBestLevel->setAnchorPoint(Vec2(0.0f, 0.5f));
	this->addChild(labelBestLevel, 12, 12);

	// Best Score	
	sprintf(textScore,"$ %d", GameManager::bestResult_Score);
	auto labelBestScore = Label::createWithTTF(textScore, FONT_FEAST_OF_FLESH_BB, 16*this->screenRatio);
	labelBestScore->setPosition(labelBestLevel_L->getPositionX(), labelBestLevel_L->getPositionY() - labelBestLevel_L->getContentSize().height*1.25f);
	labelBestScore->setColor(ccc3(255,210,2));
	labelBestScore->setAnchorPoint(Vec2(0.0f, 0.5f));
	this->addChild(labelBestScore, 13, 13);

	if (GameManager::soundOn)
		SimpleAudioEngine::getInstance()->playEffect(FAILURE_FILE, false, 1, 0, 1);	
}

void PlayingResult::fireworksEffect()
{
	//Fireworks effect
	ParticleSystem* system1 = ParticleSystemQuad::create("particles/LavaFlow.plist");

	// Set some parameters that can't be set in Particle Designer
	system1->setPositionType(kCCPositionTypeFree);
	system1->setAutoRemoveOnFinish(true);
	system1->setPosition(this->centerPoint.x + (80*this->screenRatio), this->centerPoint.y + 110*this->screenRatio);
	system1->setDuration(0.75);
	system1->setSpeed(100*this->screenRatio);
	system1->setSpeedVar(25*this->screenRatio);
	system1->setStartColor(Color4F(0,0,1,1));

	this->addChild(system1, 10, 10);

	if (GameManager::soundOn)
	{
		SimpleAudioEngine::getInstance()->playEffect(CONGRATULATIONS_FILE, false, 1, 0, 1);			
	}

	// More fireworks
	schedule(schedule_selector(PlayingResult::fireworksEffect2), 2.75f);	
}

void PlayingResult::fireworksEffect2(float dt)
{
	unschedule(schedule_selector(PlayingResult::fireworksEffect2));
	
	//Fireworks effect
	ParticleSystem* system1 = ParticleSystemQuad::create("particles/LavaFlow.plist");
	ParticleSystem* system2 = ParticleSystemQuad::create("particles/LavaFlow.plist");
	ParticleSystem* system3 = ParticleSystemQuad::create("particles/LavaFlow.plist");

	// Set some parameters that can't be set in Particle Designer
	system1->setPositionType(kCCPositionTypeFree);
	system2->setPositionType(kCCPositionTypeFree);
	system3->setPositionType(kCCPositionTypeFree);

	system1->setAutoRemoveOnFinish(true);
	system2->setAutoRemoveOnFinish(true);
	system3->setAutoRemoveOnFinish(true);

	system1->setPosition(this->centerPoint.x - (80*this->screenRatio), this->centerPoint.y + 70*this->screenRatio);
	system2->setPosition(this->centerPoint.x, this->centerPoint.y + 10*this->screenRatio);
	system3->setPosition(this->centerPoint.x + (80*this->screenRatio), this->centerPoint.y + 120*this->screenRatio);

	system1->setDuration(0.5);
	system2->setDuration(0.65);
	system3->setDuration(0.8);

	system1->setSpeed(100*this->screenRatio);
	system2->setSpeed(100*this->screenRatio);
	system3->setSpeed(100*this->screenRatio);

	system1->setSpeedVar(25*this->screenRatio);
	system2->setSpeedVar(25*this->screenRatio);
	system3->setSpeedVar(25*this->screenRatio);

	system1->setStartColor(Color4F(1,0,0,1));
	system2->setStartColor(Color4F(0,1,0,1));
	system3->setStartColor(Color4F(0,0,1,1));

	this->addChild(system1, 10, 10);
	this->addChild(system2, 11, 11);
	this->addChild(system3, 12, 12);
}

void PlayingResult::menuReplayCallback(Ref* pSender)
{
	this->pressedReplayCounter++;

	if (this->pressedReplayCounter >= 2)
	{
		this->unschedule(schedule_selector(PlayingResult::goToPlayScene));
		this->schedule(schedule_selector(PlayingResult::goToPlayScene), 0.05f);
	}
	else
	{
		if ((GameManager::bestResult_Level >= 2)
			|| (GameManager::playingCounter > 3))
		{
			#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
				if (Application::isNetworkAvailable())
				{
					//Show Google AdMob
					Application::showAd();

					this->schedule(schedule_selector(PlayingResult::goToPlayScene), 3.0f);
				} else {
					this->schedule(schedule_selector(PlayingResult::goToPlayScene), 0.05f);
				}
			#else
				//TODO: iOS, Windows Phone...
                showAdvertisment();
				this->schedule(schedule_selector(PlayingResult::goToPlayScene), 0.05f);
			#endif // CC_PLATFOR_ANDROID

		} else {
			this->schedule(schedule_selector(PlayingResult::goToPlayScene), 0.05f);
		}
	}


	
//	//for Windows Phone
//	this->schedule(schedule_selector(PlayingResult::goToPlayScene), 0.05f);
//
//	if (GameManager::soundOn)
//	{
//		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
//	}

//	Application::showAdBuddiz();

//	CCTransitionFade* transition = CCTransitionFade::create(1, Playing::createScene(), ccBLACK);
//	CCDirector::getInstance()->replaceScene(transition);
	
}

void PlayingResult::menuShareCallback(Ref* pSender)
{
	if (GameManager::soundOn) 
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}


    this->capture();
    
}


void PlayingResult::menuHomeCallback(Ref* pSender)
{
	if (GameManager::soundOn)
	{
		SimpleAudioEngine::getInstance()->playEffect(BUTTON_TOUCHED_FILE_1, false, 1, 0, 1);
	}

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//	// Hide Google Ad
//	Application::hideAd();
//#else
//	//TODO...
//#endif // CC_PLATFOR_ANDROID

	CCTransitionFade* transition = CCTransitionFade::create(1, Home::createScene(), ccBLACK);
    CCDirector::getInstance()->replaceScene(transition);
}

void PlayingResult::afterCaptured(bool succeed, const std::string& outputFile)
{
    if (succeed)
    {
        // show screenshot
        auto sp = Sprite::create(outputFile);
        this->addChild(sp, 111, 111);
        sp->setPosition(this->centerPoint);
        //sp->setScale(0.25);
		
		ActionInterval *scaleTo = ScaleTo::create(0.1f, 1.15f);
		ActionInterval *scaleToBack = ScaleTo::create(0.25, 0.0f);
		Sequence *actSequence = (Sequence*)Sequence::create(scaleTo, CCDelayTime::create(0.1f), scaleToBack,  NULL); //CCDelayTime::create(1.0f), fadeOut,

		sp->runAction(actSequence);

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

		Application::getInstance()->setScrShotPathToJava(outputFile.c_str());
    
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    
		// TODO 
        shareFacebook(outputFile.c_str());
#endif

		
    }
    else
    {
        log("Capture screen failed.");
    }
}

void PlayingResult::capture()
{
    utils::captureScreen(CC_CALLBACK_2(PlayingResult::afterCaptured, this), SCREENSHOT_FILE_PATH); //"/sdcard/CaptureScreen.jpg" .png
}


//void PlayingResult::showExcellentSeal(float dt)
//{
//	unschedule(schedule_selector(PlayingResult::showExcellentSeal));
//
//	// ----- add Excellent
//    auto spriteExcellent = Sprite::create(s_excellent);
//
//    // position the sprite on the center of the screen
//    spriteExcellent->setPosition(this->screenSize.width - spriteExcellent->getContentSize().width*0.5,
//									this->centerPoint.y + spriteExcellent->getContentSize().height*0.85);
//	spriteExcellent->setScale(3.0f);
//
//    // add the sprite as a child to this layer
//    this->addChild(spriteExcellent, 10, 10);
//
//	spriteExcellent->runAction(CCScaleTo::create(0.25, 1.0f));
//
//	schedule(schedule_selector(PlayingResult::playSealSound), 0.25f);
//}

void PlayingResult::goToPlayScene(float dt)
{
	unschedule(schedule_selector(PlayingResult::goToPlayScene));

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//	// Hide Google Ad
//	Application::hideAd();
//#else
//	//TODO...
//#endif // CC_PLATFOR_ANDROID

	CCTransitionFade* transition = CCTransitionFade::create(1, Playing::createScene(), ccBLACK);
	CCDirector::getInstance()->replaceScene(transition);
}
