#include "AppDelegate.h"
#include "AppMacros.h"
#include "StartupScene.h"
#include "Common/GameManager.h"

#include <vector>
#include <string>

USING_NS_CC;
using namespace std;
using namespace CocosDenshion;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
//	// Hide Google Ad
//	Application::hideAd();
//#else
//	//TODO...
//#endif // CC_PLATFOR_ANDROID


    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLView::create("Angry Birds' Eggs");
		//glview->setFrameSize(360, 640); // for Win32 testing only
		////glview->setFrameSize(480, 800);
		//glview->setFrameSize(320, 480);
        director->setOpenGLView(glview);
    }

	Size frameSize = glview->getFrameSize();
	designResolutionSize = frameSize; //get from real device'size

	// Set the design resolution
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
    // a bug in DirectX 11 level9-x on the device prevents ResolutionPolicy::NO_BORDER from working correctly
    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::SHOW_ALL);
#else
    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::NO_BORDER);
#endif


    
    vector<string> searchPath;

    // In this demo, we select resource according to the frame's height.
    // If the resource size is different from design resolution size, you need to set contentScaleFactor.
    // We use the ratio of resource's height to the height of design resolution,
    // this can make sure that the resource's height could fit for the height of design resolution.

	float tmpF = director->getContentScaleFactor();

    // if the frame's height is larger than the height of medium resource size, select large resource.
	if (frameSize.height > resource_800x1280.size.height)
	{
        //searchPath.push_back(resource_1536x2048.directory);
		searchPath.push_back(resource_800x1280.directory);

		designResolutionSize = CCSize(resource_800x1280.size.width, resource_800x1280.size.height);
		glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::SHOW_ALL);

		director->setContentScaleFactor(MIN(resource_800x1280.size.height/designResolutionSize.height, resource_800x1280.size.width/designResolutionSize.width));
        //director->setContentScaleFactor(MIN(resource_1536x2048.size.height/designResolutionSize.height, resource_1536x2048.size.width/designResolutionSize.width));
	}
	else if (frameSize.height > resource_480x800.size.height)
    {
        searchPath.push_back(resource_800x1280.directory);
        
        director->setContentScaleFactor(MIN(resource_800x1280.size.height/designResolutionSize.height, resource_800x1280.size.width/designResolutionSize.width));
    }
	else if (frameSize.height > resource_320x480.size.height)
    {
        searchPath.push_back(resource_480x800.directory);
        
        director->setContentScaleFactor(MIN(resource_480x800.size.height/designResolutionSize.height, resource_480x800.size.width/designResolutionSize.width));
		tmpF = director->getContentScaleFactor();
    }
    // if the frame's height is smaller than the height of medium resource size, select small resource.
	else
    {
        searchPath.push_back(resource_320x480.directory);

        director->setContentScaleFactor(MIN(resource_320x480.size.height/designResolutionSize.height, resource_320x480.size.width/designResolutionSize.width));
		
    }
    
    // set searching path
    FileUtils::getInstance()->setSearchPaths(searchPath);

    // turn on display FPS
    //director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    auto scene = Startup::createScene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();
	
	//applicationDidEnterBackground 
#if (CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID)
    Director::getInstance()->pause();

#else
    Director::getInstance()->pause();
    Director::getInstance()->stopAnimation();
#endif

    if (GameManager::backgroundMusicOn)
    {
    	//if you use SimpleAudioEngine, it must be pause
        SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    }

}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

	//applicationWillEnterForeground 
#if (CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID)
    Director::getInstance()->resume();
#else
    Director::getInstance()->resume();
    Director::getInstance()->startAnimation();
#endif


    if (GameManager::backgroundMusicOn)
	{
		// if you use SimpleAudioEngine, it must resume here
		SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	}
}
