#ifndef _RESOURCE_NAMES_H_
#define _RESOURCE_NAMES_H_

// android effect only support ogg
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    #define BUTTON_TOUCHED_FILE_1       "sounds/tap_08.ogg"
	#define BUTTON_TOUCHED_FILE_2       "sounds/tap_06.ogg"
	#define ITEM_SELECT_FILE			"sounds/tap_03.ogg"
	#define BOMB_EFFECT_FILE_1			"sounds/bomb_1.ogg"
	#define BOMB_EFFECT_FILE_2			"sounds/bomb_2.ogg"
	#define DIAMOND_EFFECT_FILE_1		"sounds/score_11.ogg"
	#define GEMSTONE_EFFECT_FILE_1		"sounds/score_09.ogg"
	#define GOLD_EFFECT_FILE_1			"sounds/score_08.ogg"
	#define GOLD_EFFECT_FILE_2			"sounds/score_10.ogg"
	#define STONE_EFFECT_FILE			"sounds/score_02.ogg"
	#define CONGRATULATIONS_FILE		"sounds/congratulations-spoken.ogg"
	#define COMPLETED_FILE				"sounds/completed.ogg"
	#define LEVEL_UP_FILE				"sounds/level_up.ogg"
	#define LEVEL_UP_QUICK_FILE			"sounds/level_up_quick.ogg"
	#define FAILURE_FILE				"sounds/failure.ogg"
	#define LET_GO_FILE					"sounds/go_01.ogg"
	#define PIN_STAR_FILE_01			"sounds/pin_star_01.ogg"
#elif( CC_TARGET_PLATFORM == CC_PLATFORM_MARMALADE)
    #define BUTTON_TOUCHED_FILE_1		"sounds/tap_08.raw"
	#define BUTTON_TOUCHED_FILE_2		"sounds/tap_06.raw"
	#define ITEM_SELECT_FILE			"sounds/tap_03.raw"
	#define BOMB_EFFECT_FILE_1			"sounds/bomb_1.raw"
	#define BOMB_EFFECT_FILE_2			"sounds/bomb_2.raw"
	#define DIAMOND_EFFECT_FILE_1		"sounds/score_11.raw"
	#define GEMSTONE_EFFECT_FILE_1		"sounds/score_09.raw"
	#define GOLD_EFFECT_FILE_1			"sounds/score_08.raw"
	#define GOLD_EFFECT_FILE_2			"sounds/score_10.raw"
	#define STONE_EFFECT_FILE			"sounds/score_02.raw"
	#define CONGRATULATIONS_FILE		"sounds/congratulations-spoken.raw"
	#define COMPLETED_FILE				"sounds/completed.raw"
	#define LEVEL_UP_FILE				"sounds/level_up.raw"
	#define LEVEL_UP_QUICK_FILE			"sounds/level_up_quick.raw"
	#define FAILURE_FILE				"sounds/failure.raw"
	#define LET_GO_FILE					"sounds/go_01.raw"
	#define PIN_STAR_FILE_01			"sounds/pin_star_01.raw"
#else
    #define BUTTON_TOUCHED_FILE_1		"sounds/tap_08.wav"
	#define BUTTON_TOUCHED_FILE_2		"sounds/tap_06.wav"
	#define ITEM_SELECT_FILE			"sounds/tap_03.wav"
	#define BOMB_EFFECT_FILE_1			"sounds/bomb_1.wav"
	#define BOMB_EFFECT_FILE_2			"sounds/bomb_2.wav"
	#define DIAMOND_EFFECT_FILE_1		"sounds/score_11.wav"
	#define GEMSTONE_EFFECT_FILE_1		"sounds/score_09.wav"
	#define GOLD_EFFECT_FILE_1			"sounds/score_08.wav"
	#define GOLD_EFFECT_FILE_2			"sounds/score_10.wav"
	#define STONE_EFFECT_FILE			"sounds/score_02.wav"
	#define CONGRATULATIONS_FILE		"sounds/congratulations-spoken.wav"
	#define COMPLETED_FILE				"sounds/completed.wav"
	#define LEVEL_UP_FILE				"sounds/level_up.wav"
	#define LEVEL_UP_QUICK_FILE			"sounds/level_up_quick.wav"
	#define FAILURE_FILE				"sounds/failure.wav"
	#define LET_GO_FILE					"sounds/go_01.wav"
	#define PIN_STAR_FILE_01			"sounds/pin_star_01.wav"
#endif // CC_PLATFOR_ANDROID

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
    #define BG_MUSIC_FILE				"sounds/bg_music_01.wav"
	#define BG_MUSIC_PLAYING_FILE       "sounds/bg_music_02.wav"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
    #define BG_MUSIC_FILE				"sounds/bg_music_01.wav"
	#define BG_MUSIC_PLAYING_FILE       "sounds/bg_music_02.wav"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_BLACKBERRY || CC_TARGET_PLATFORM == CC_PLATFORM_LINUX )
    #define BG_MUSIC_FILE				"sounds/bg_music_01.ogg"
	#define BG_MUSIC_PLAYING_FILE       "sounds/bg_music_02.ogg"
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    #define BG_MUSIC_FILE				"sounds/bg_music_01.wav" //.caf
	#define BG_MUSIC_PLAYING_FILE       "sounds/bg_music_02.wav" //.caf
#else
    #define BG_MUSIC_FILE				"sounds/bg_music_01.mp3"
	#define BG_MUSIC_PLAYING_FILE       "sounds/bg_music_02.mp3"
#endif // CC_PLATFOR_WIN32

#if ((CC_TARGET_PLATFORM == CC_PLATFORM_IOS) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC))
    // custom ttf files are defined in Test-info.plist
#define FONT_ALPHA_MALE_MODERN		"fonts/AlphaMaleModern.ttf"
#define FONT_BINHTUY				"fonts/BINHTUY.ttf"
#define FONT_DR_EVE_L				"fonts/Dr Eve L.ttf"
#define FONT_EL_ABOGADO_LOCO		"fonts/El_Abogado_Loco.ttf"
#define FONT_FORTUNE_COOKIE_NF		"fonts/FortuneCookieNF.ttf"
#define FONT_MARKER_FELT			"fonts/Marker Felt.ttf"
//#define FONT_FEAST_OF_FLESH_I		"fonts/FEASFBI.ttf"
#define FONT_FEAST_OF_FLESH_BB		"fonts/FEASFBRG.ttf"
#define FONT_OLD_EVILS				"fonts/old evils.ttf"
#else
    #define FONT_ALPHA_MALE_MODERN		"fonts/AlphaMaleModern.ttf"
    #define FONT_BINHTUY				"fonts/BINHTUY.ttf"
    #define FONT_DR_EVE_L				"fonts/Dr Eve L.ttf"
    #define FONT_EL_ABOGADO_LOCO		"fonts/El_Abogado_Loco.ttf"
    #define FONT_FORTUNE_COOKIE_NF		"fonts/FortuneCookieNF.ttf"
    #define FONT_MARKER_FELT			"fonts/Marker Felt.ttf"
	//#define FONT_FEAST_OF_FLESH_I		"fonts/FEASFBI.ttf"
	#define FONT_FEAST_OF_FLESH_BB		"fonts/FEASFBRG.ttf"
	#define FONT_OLD_EVILS				"fonts/old evils.ttf"

#endif

static const char s_ozenes_logo[]			= "ozenes_logo.png";
static const char s_ozenes_logo_3d[]		= "ozenes_logo_3d.png";
static const char s_angry_birds_eggs[]		= "msg/angry_birds_eggs.png";

static const char s_nest_bird_1[]			= "nests/nest1.png";
static const char s_nest_bird_2[]			= "nests/nest2.png";
static const char s_nest_bird_3[]			= "nests/nest3.png";

static const char s_diamond_egg[]			= "eggs/diamond_egg.png";
static const char s_gemstone_egg[]			= "eggs/gems_egg.png";
static const char s_gold_egg_1[]            = "eggs/golden_egg.png";
static const char s_gold_egg_2[]            = "eggs/golden_egg.png";
static const char s_stone_egg[]             = "eggs/stone_egg.png";
static const char s_bomb_egg[]				= "eggs/bomb_egg.png";

static const char s_pig[]					= "pig/pig.png";
static const char s_pig_ear_eyes1[]			= "pig/pig_ear_eyes1.png";
static const char s_pig_ear_eyes2[]			= "pig/pig_ear_eyes2.png";
static const char s_pig_ear_eyes3[]			= "pig/pig_ear_eyes3.png";
static const char s_pig_ear_eyes4[]			= "pig/pig_ear_eyes4.png";
static const char s_pig_ear_eyes5[]			= "pig/pig_ear_eyes5.png";

static const char s_pig1[]					= "pig/pig1.png";
static const char s_pig2[]					= "pig/pig2.png";
static const char s_pig3[]					= "pig/pig3.png";
static const char s_pig4[]					= "pig/pig4.png";
static const char s_pig5[]					= "pig/pig5.png";


// Price (in USD)
static const char s_score_5000[]			= "scores/5000.png";
static const char s_score_2000[]			= "scores/2000.png";
static const char s_score_1000[]            = "scores/1000.png";
static const char s_score_0020[]            = "scores/20.png";

static const char s_bg_home[]				= "bg_home.jpg";
static const char s_bg_black[]				= "bg_black.png";
static const char s_bg_option[]				= "bg_common.jpg";
static const char s_bg_result[]             = "bg_result.jpg";
static const char s_bg_result_failed[]      = "bg_result_failed.jpg";
static const char s_bg_common[]				= "bg_common.jpg";
static const char s_bg_playing[]			= "bg_playing.jpg";

static const char s_dialog_bg[]				= "dialog_bg.png";

static const char s_btn_home[]				= "buttons/btn_home.png";
static const char s_btn_home_tapped[]		= "buttons/btn_home_tapped.png";

static const char s_btn_play[]				= "buttons/btn_play.png";
static const char s_btn_play_tapped[]		= "buttons/btn_play_tapped.png";

static const char s_btn_go[]				= "buttons/btn_go.png";
static const char s_btn_go_tapped[]			= "buttons/btn_go_tapped.png";

static const char s_btn_option[]			= "buttons/btn_option.png";
static const char s_btn_option_tapped[]		= "buttons/btn_option_tapped.png";
static const char s_btn_more[]				= "buttons/btn_more.png";
static const char s_btn_more_tapped[]		= "buttons/btn_more_tapped.png";

static const char s_btn_replay[]			= "buttons/btn_replay.png";
static const char s_btn_replay_tapped[]		= "buttons/btn_replay_tapped.png";
static const char s_btn_share[]				= "buttons/btn_share.png";
static const char s_btn_share_tapped[]		= "buttons/btn_share_tapped.png";

static const char s_btn_left[]				= "buttons/btn_left.png";
static const char s_btn_left_tapped[]		= "buttons/btn_left_tapped.png";
static const char s_btn_right[]				= "buttons/btn_right.png";
static const char s_btn_right_tapped[]		= "buttons/btn_right_tapped.png";

static const char s_btn_sound_on[]			= "buttons/btn_sound_on.png";
static const char s_btn_sound_off[]			= "buttons/btn_sound_off.png";
static const char s_btn_bg_music_on[]		= "buttons/btn_music_on.png";
static const char s_btn_bg_music_off[]		= "buttons/btn_music_off.png";

static const char s_btn_about[]				= "buttons/btn_about.png";
static const char s_btn_about_tapped[]		= "buttons/btn_about_tapped.png";
static const char s_btn_best_score[]		= "buttons/btn_best_score.png";
static const char s_btn_best_score_tapped[]	= "buttons/btn_best_score_tapped.png";

static const char s_btn_yes[]				= "buttons/btn_yes.png";
static const char s_btn_yes_tapped[]		= "buttons/btn_yes_tapped.png";
static const char s_btn_no[]				= "buttons/btn_no.png";
static const char s_btn_no_tapped[]			= "buttons/btn_no_tapped.png";

static const char s_btn_gift[]				= "buttons/btn_gift.png";
static const char s_btn_gift_tapped[]		= "buttons/btn_gift_tapped.png";


static const char s_mn_bg[]					= "buttons/mn_bg.png";	

static const char s_top_progress_bar_bg[]			= "progress_bar_bg.png";
static const char s_top_progress_bar_back[]			= "progress_bar_back.png";
static const char s_top_progress_bar_front[]		= "progress_bar_front.png";

static const char s_theme_borber[]					= "theme_borber.png";

static const char s_buy_full_version[]				= "buy_full_version.png";

static const char s_star_effect[]					= "particles/stars.png";
//static const char s_star2_effect[]				= "stars/small_star.png";
static const char s_star3_effect[]					= "stars/small_star40.png";


static const char s_about_info[]					= "about.png";

//static const char s_game_over[]						= "msg/game_over.png";
static const char s_level_up_playing[]				= "msg/level_up.png";

static const char s_msg_failed[]					= "Failed!";
static const char s_msg_level_up[]					= "Level Up";
static const char s_msg_new_best[]					= "New Best";
static const char s_msg_best[]						= "Best";

static const char s_excellent[]						= "msg/excellent.png";
static const char s_new_best[]						= "msg/new_best.png";
static const char s_my_best[]						= "msg/my_best.png";
static const char s_level_new_best[]				= "msg/level_new_best.png";
static const char s_level_best[]					= "msg/level_best.png";
static const char s_level_failed[]					= "msg/level_failed.png";
static const char s_failed[]						= "msg/failed.png";
static const char s_rate_this_game[]				= "msg/rate_this_game.png";

static const char s_star_banner[]					= "stars/star_banner.png";
static const char s_small_star[]					= "stars/small_star.png";
static const char s_small_star_white[]				= "stars/small_star40.png";
static const char s_star1[]							= "stars/star1.png";
static const char s_star2[]							= "stars/star2.png";
static const char s_star3[]							= "stars/star3.png";
static const char s_star2_dark[]					= "stars/star2_dark.png";
static const char s_star3_dark[]					= "stars/star3_dark.png";




#endif
