#ifndef GOLDRUSH_GAMEMANAGER_H
#define GOLDRUSH_GAMEMANAGER_H

#include "cocos2d.h"



USING_NS_CC ;

typedef struct tagResource
{
    cocos2d::Size size;
    char directory[100];
} Resource;

static Resource resource_320x480  =  { cocos2d::Size(320,480), "320x480" };
static Resource resource_480x800  =  { cocos2d::Size(480,800), "480x800" };
//static Resource resource_768x1024  =  { cocos2d::Size(768,1024), "768x1024" };
static Resource resource_800x1280  =  { cocos2d::Size(800,1280), "800x1280" };
static Resource resource_1536x2048  =  { cocos2d::Size(1536,2048), "1536x2048" };


class GameManager : public cocos2d::CCObject
{
private:
    //Constructor
    //GameManager();
    
    //Instance of the singleton
    //static GameManager* m_mySingleton;
    
public:    
    //Get instance of singleton
    //static GameManager* sharedGameManager();    
	
	//static GamePlayKinds gamePlayKinds;

	static bool soundOn;
	static bool backgroundMusicOn;

	static int themeBg; // 1: s_bg_playing_01; 2: s_bg_playing_02;..

	static int uniqueRandom(int maxNumber, int maxRandom, int radius);

	static int diamondEgg_Tag;
	static int gemsEgg_Tag;
	static int goldEgg_Tag;	
	static int stoneEgg_Tag;
	static int bombEgg_Tag;
	static int nestBird_Tag;

	static int numberOfColumns;
	static int numberOfRows;

	static int bestResult_Level;
	static int bestResult_Score;

	static int playingLevel;
	static int playingScore;

	static int playingCounter;

	static bool isFailedGame;

	static float calculateDeltaTime( struct timeval *lastUpdate );

	static int getTagByIdx(int objIdx);
	static std::string getFileNameByIdx(int objIdx);
	static std::string getUSDFileNameByTag(int objTag);
	static std::string getGoldSoundFileNameByTag(int goldTag);

	static int calculateLevelTarget(int level );
	static float calculateSpeedForwardOfPig(int level );
};

#endif
