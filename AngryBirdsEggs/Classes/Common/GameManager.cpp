
#include "GameManager.h" 
#include "ResourceNames.h"

using namespace cocos2d;


////All static variables need to be defined in the .cpp file
////I've added this following line to fix the problem
//GameManager* GameManager::m_mySingleton = NULL;
//
//GameManager::GameManager()
//{    
//    
//}
//
//GameManager* GameManager::sharedGameManager()
//{
//    //If the singleton has no instance yet, create one
//    if(NULL == m_mySingleton)
//    {
//        //Create an instance to the singleton
//        m_mySingleton = new GameManager();
//    }
//    
//    //Return the singleton object
//    return m_mySingleton;
//}

 //GamePlayKinds GameManager::gamePlayKinds = GamePlayKind_UNKNOWN;

 bool GameManager::soundOn = true;
 bool GameManager::backgroundMusicOn = true;
 int GameManager::themeBg = 1;

 
 int GameManager::diamondEgg_Tag = 5000; 
 int GameManager::gemsEgg_Tag = 2000;
 int GameManager::goldEgg_Tag = 1000;
 int GameManager::stoneEgg_Tag = 20;
 int GameManager::bombEgg_Tag = -1;
 int GameManager::nestBird_Tag = -1000;

 int GameManager::numberOfColumns = 3;
 int GameManager::numberOfRows = 3;

 int GameManager::bestResult_Level = 1;
 int GameManager::bestResult_Score  = 0;

 int GameManager::playingLevel = 1;
 int GameManager::playingScore = 0;

 int GameManager::playingCounter = 0;

 bool GameManager::isFailedGame = false;


 int GameManager::uniqueRandom(int maxNumber, int maxRandom, int radius)
 {
	 
	int result = (int)((rand() % (maxRandom - 1) + 1) * (maxNumber/maxRandom)) + (rand() % radius/3)*CCRANDOM_MINUS1_1();

	return result;
 }

 float GameManager::calculateDeltaTime( struct timeval *lastUpdate )
{
    struct timeval now;

    gettimeofday( &now, nullptr);

    float dt = (now.tv_sec - lastUpdate->tv_sec) + (now.tv_usec - lastUpdate->tv_usec) / 1000000.0f;

    return dt;
}


 
int GameManager::getTagByIdx(int objIdx)
{
	int resultTag = 0;

	switch (objIdx) {
            case 1:
				resultTag = GameManager::diamondEgg_Tag; //diamond
				break;
            case 2:
            case 3:
                resultTag = GameManager::gemsEgg_Tag; //gems
                break;
			case 4:
            case 5:
            case 6:
			case 7:
				resultTag = GameManager::goldEgg_Tag; //gold
				break;
            case 8:
            case 9:
            case 10:
			case 11:
			case 12:
                resultTag = GameManager::stoneEgg_Tag; //stone
                break;			
            default:
				resultTag = GameManager::bombEgg_Tag;  //bomb: -1;
                break;
        }

	return resultTag;
}

std::string GameManager::getFileNameByIdx(int objIdx)
{
	std::string resultName;

	switch (objIdx) {
            case 1:
				resultName = s_diamond_egg; //5000
				break;
            case 2:
            case 3:
                resultName = s_gemstone_egg; //2000
                break;
			case 4:
            case 5:
                resultName = s_gold_egg_1; //1000
                break;
            case 6:
			case 7:
				resultName = s_gold_egg_2; //1000
				break;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                resultName = s_stone_egg; //20
                break;
            default:
				resultName = s_bomb_egg; //-1000
                break;
        }

	return resultName; //.c_str();
}


std::string GameManager::getUSDFileNameByTag(int objTag)
{
	std::string resultName;

	switch (objTag) {
            case 5000:
				resultName = s_score_5000;
				break;
            case 2000:
                resultName = s_score_2000;
                break;
			case 1000:
				resultName = s_score_1000;
				break;
            case 20:
                resultName = s_score_0020;
                break;
            default:
                break;
        }

	return resultName; //.c_str();
}


 std::string GameManager::getGoldSoundFileNameByTag(int goldTag)
{
	std::string resultName = GOLD_EFFECT_FILE_2;

	switch (goldTag) {
        case 5000:
			resultName = DIAMOND_EFFECT_FILE_1; 
			break;
		case 2000:
            resultName = GEMSTONE_EFFECT_FILE_1;
            break;		                   
		case 1000:
		{
			char text[256];	
			int randIdx = (rand() % 2) + 1;
			if (randIdx > 2)
				randIdx = 2;
			if (randIdx == 1)
				resultName = GOLD_EFFECT_FILE_1;
			else
				resultName = GOLD_EFFECT_FILE_2;
            break;
		}
		case 20:
			resultName = STONE_EFFECT_FILE;
			break;
		default:
			break;
    }


	return resultName; //.c_str();
}

 int GameManager::calculateLevelTarget(int level )
 {
	 int resultScore = 0;

	 for (int i = 1; i <= level; i++)
	 {
		 switch (i) {
			 case 1:
				 resultScore = 30000;
				break;
			 case 2:
				 resultScore = 50000;
				 break;
			 case 3:
				 resultScore = 70000;
				 break;
			 case 4:
				 resultScore = 100000;
				 break;
			 default:
				 resultScore = resultScore + 50000;
				 break;
		 }
	 }

	return resultScore;
 }

 float GameManager::calculateSpeedForwardOfPig(int level )
 {
	 float resultSpeed = 0;

	 if (level <= 2)
		 resultSpeed = -2.5; //-3.0;
	 else if (level <= 6)
		 resultSpeed = -3.0; //-3.5;
	 else if (level <= 10)
		 resultSpeed = -3.5; //-3.5;
	 else if (level <= 15)
		 resultSpeed = -4.0; //-4.0;
	 else if (level <= 20)
		 resultSpeed = -4.5; //-4.5;
	 else if (level <= 25)
	 	 resultSpeed = -5.0; //-5.0;
	 else if (level <= 30)
	 	 resultSpeed = -5.5;
	 else if (level <= 35)
	 	 resultSpeed = -6.0;
	 else if (level <= 40)
	 	 resultSpeed = -6.5;
	 else if (level <= 45)
	 	 resultSpeed = -7.0;
	 else if (level <= 50)
	 	 resultSpeed = -7.5;
	 else if (level <= 55)
	 	 resultSpeed = -8.0;
	 else if (level <= 60)
	 	 resultSpeed = -8.5;
	 else if (level <= 65)
	 	 resultSpeed = -9.0;
	 else
		 resultSpeed = -10.0;


	return resultSpeed;
 }


