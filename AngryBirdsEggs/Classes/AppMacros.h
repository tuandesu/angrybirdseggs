#ifndef __APPMACROS_H__
#define __APPMACROS_H__

#include "cocos2d.h"

/* For demonstrating using one design resolution to match different resources,
   or one resource to match different design resolutions.

   [Situation 1] Using one design resolution to match different resources.
     Please look into Appdelegate::applicationDidFinishLaunching.
     We check current device frame size to decide which resource need to be selected.
     So if you want to test this situation which said in title '[Situation 1]',
     you should change ios simulator to different device(e.g. iphone, iphone-retina3.5, iphone-retina4.0, ipad, ipad-retina),
     or change the window size in "proj.XXX/main.cpp" by "CCEGLView::setFrameSize" if you are using win32 or linux plaform
     and modify "proj.mac/AppController.mm" by changing the window rectangle.

   [Situation 2] Using one resource to match different design resolutions.
     The coordinates in your codes is based on your current design resolution rather than resource size.
     Therefore, your design resolution could be very large and your resource size could be small.
     To test this, just define the marco 'TARGET_DESIGN_RESOLUTION_SIZE' to 'DESIGN_RESOLUTION_2048X1536'
     and open iphone simulator or create a window of 480x320 size.

   [Note] Normally, developer just need to define one design resolution(e.g. 960x640) with one or more resources.
 */

#define DESIGN_RESOLUTION_320x480    0	// Smartphone (popular)                     --> OK
#define DESIGN_RESOLUTION_360x640    1	// Smartphone (for Win32 testing only)
#define DESIGN_RESOLUTION_400x800	 2	// Smartphone (LG G2x 4G)
#define DESIGN_RESOLUTION_480x800    3	// Smartphone (popular)                     --> OK
#define DESIGN_RESOLUTION_540x960	 5	// Smartphone (HTC, Motorola)
#define DESIGN_RESOLUTION_640x960    5	// Smartphone (iPhone 4)                    
#define DESIGN_RESOLUTION_600x1024	 6  // Tablet (Amazon Kindle Fire)
#define DESIGN_RESOLUTION_768x1024   7  // Tablet (iPad 1+2)                        
#define DESIGN_RESOLUTION_640x1136   8  // Smartphone (iPhone 5)
#define DESIGN_RESOLUTION_720x1280   9  // Smartphone (Zenfone 5)
#define DESIGN_RESOLUTION_800x1280   10  // Tablet (Acer, Lenovo ThinkPad)          --> OK
#define DESIGN_RESOLUTION_1536x2048  11  // Tablet (iPad 3)                         --> OK

/* If you want to switch design resolution, change next line */
#define TARGET_DESIGN_RESOLUTION_SIZE  DESIGN_RESOLUTION_480X320

//typedef struct tagResource
//{
//    cocos2d::Size size;
//    char directory[100];
//}Resource;

//static Resource smallResource  =  { cocos2d::Size(480, 320),   "iphone" };
//static Resource mediumResource =  { cocos2d::Size(1024, 768),  "ipad"   };
//static Resource largeResource  =  { cocos2d::Size(2048, 1536), "ipadhd" };




//#if (TARGET_DESIGN_RESOLUTION_SIZE == DESIGN_RESOLUTION_480X320)
//static cocos2d::Size designResolutionSize = cocos2d::Size(480, 320);
//#elif (TARGET_DESIGN_RESOLUTION_SIZE == DESIGN_RESOLUTION_1024X768)
//static cocos2d::Size designResolutionSize = cocos2d::Size(1024, 768);
//#elif (TARGET_DESIGN_RESOLUTION_SIZE == DESIGN_RESOLUTION_2048X1536)
//static cocos2d::Size designResolutionSize = cocos2d::Size(2048, 1536);
//#else
//#error unknown target design resolution!
//#endif
static cocos2d::Size designResolutionSize = cocos2d::Size(320,480); //cocos2d::Size(1024, 768);

// The font size 24 is designed for small resolution, so we should change it to fit for current design resolution
#define TITLE_FONT_SIZE  (cocos2d::Director::getInstance()->getOpenGLView()->getDesignResolutionSize().width / resource_480x320.size.width * 24) //smallResource

#endif /* __APPMACROS_H__ */
