#ifndef __ABOUT_SCENE_H__
#define __ABOUT_SCENE_H__

#include "cocos2d.h"
USING_NS_CC;

class About : public cocos2d::Layer
{
//private:


public:
	
	Size screenSize;
	//Size visibleSize;
	Point centerPoint;

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callback: Classic
    void menuHomeCallback(cocos2d::Ref* pSender);

    
    // implement the "static create()" method manually
    CREATE_FUNC(About);
};

#endif // __ABOUT_SCENE_H__
